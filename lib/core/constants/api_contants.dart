class ApiConstants {
  final baseUrl = "http://192.168.1.77:3020";
  // final baseUrl = "http://192.168.1.47:3000";
  // final baseUrl = "https://api.empire.awessome.fr";
  final uploadFile = "/file";
  final uploadFiles = "/files";
  final signIn = "/auth/signin";
  final signUp = "/auth/signup";
  final checkEmail = "/auth/check";
  final resetPassword = "/auth/reset";
  final refreshToken = "/auth/renew";
  final currentUser = "/users/me";
  final userPermissions = "/users/permissions";
  final adherents = "/adherents";
  final notifications = "/notifications";
  final categories = "/categories";
  final addPoints = "/adherents/add-points";
  final users = "/users";
  final conversations = "/chat/conversations";
  final userConversations = "/users/conversations";
  final chatMessages = "/chat/messages";
  final allRepresentants = "/users/representants";
  final projects = "/projects";
  final articles = "/articles";
  final vibes = "/vibes";
  final likeVibe = "/vibes/like";
  final discoverProfiles = "/discover/profiles";
}
