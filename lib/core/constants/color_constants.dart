import 'package:flutter/material.dart';

class ColorConstants {
  static const gradient = [Color(0xff6b54e9), Color(0xff31a7fa)];
  Color primary = const Color(0xff61338A);
  Color discover = const Color(0xffF2BA99);
  Color collaborate = const Color(0xff14B5B5);
  Color record = const Color(0xff61338A);
  Color vibes = const Color(0xff61338A);
  Color settings = const Color(0xff007DA6);
}