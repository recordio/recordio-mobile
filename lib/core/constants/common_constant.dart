import 'package:flutter/material.dart';

class CommonConstant {
  final appBarSize = AppBar().preferredSize.height;
  final headerSize = AppBar().preferredSize.height;
  final audioSupported = ['mp3', 'aac', 'flac', 'wav', 'ogg'];
  final cardHeight = 386.0;
  final cardWidth = 250.0;
  final cardBottomPadding = 12.0;
  final imageMaxHeight = 1080.0;
  final imageMaxWidth = 1080.0;
  final imageQuality = 50;

  // vibes
  final leftPadding = 10.0;
}