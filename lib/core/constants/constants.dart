
import 'package:recordio/core/constants/api_contants.dart';
import 'package:recordio/core/constants/color_constants.dart';
import 'package:recordio/core/constants/common_constant.dart';
import 'package:recordio/core/constants/keys_constant.dart';
import 'package:recordio/core/constants/shared_preferences_constants.dart';
import 'package:recordio/core/constants/text_style_constants.dart';

class Const {
  static KeysConstant keys = KeysConstant();
  static final ApiConstants api = ApiConstants();
  static final color = ColorConstants();
  static CommonConstant common = CommonConstant();
  static SharedPreferencesConstants sharedPrefs = SharedPreferencesConstants();
  // static MqttConstants mqtt = MqttConstants();
  // static ChatConstant chat = ChatConstant();
  static TextStylesConstant style = TextStylesConstant();
}
