
import 'package:flutter/material.dart';

class TextStylesConstant {
  final pageTitle = const TextStyle(fontSize: 48, fontWeight: FontWeight.w100);
  final normal = const TextStyle(fontSize: 14, fontWeight: FontWeight.w300);
  final title = const TextStyle(fontSize: 14, fontWeight: FontWeight.w500);
  final like = const TextStyle(fontSize: 10, fontWeight: FontWeight.bold);
}