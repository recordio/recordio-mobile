enum MessageType { text, audio, video, link, gif }

extension MessageTypeExtension on MessageType {
  String get value {
    switch (this) {
      case MessageType.text:
        return 'text';
      case MessageType.audio:
        return 'audio';
      case MessageType.video:
        return 'video';
      case MessageType.link:
        return 'link';
      case MessageType.gif:
        return 'gif';
      default:
        return 'text';
    }
  }
}
