enum Onboarding { unInitialized, profileSetup, samplesSetup, genreSetup, completed }

enum Progression { uninitialized, uploading, finished, error }
