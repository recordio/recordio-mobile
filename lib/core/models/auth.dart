class Auth {
  String id;
  String token;

  Auth({
    required this.id,
    required this.token,
  });

  factory Auth.fromMap(Map<String, dynamic> map) {

    return Auth(
      id: map['id'] as String,
      token: map['token'] as String,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'id': id,
      'token': token,
    };
  }
}
