
class Conversation {
  String id;
  String otherUserId;
  String? title;
  String? imageUrl;

  Conversation({
    required this.id,
    required this.otherUserId,
    required this.title,
    required this.imageUrl,
  });

  Map<String, dynamic> toMap() {
    return {
      'id': id,
      'otherUserId': otherUserId,
      'title': title,
      'imageUrl': imageUrl,
    };
  }

  factory Conversation.fromMap(Map<String, dynamic> map) {
    return Conversation(
      id: map['id'] as String,
      otherUserId: map['otherUserId'] as String,
      title: map['title'] != null ? map['title'] as String : null,
      imageUrl: map['imageUrl'] != null ? map['imageUrl'] as String : null,
    );
  }
}
