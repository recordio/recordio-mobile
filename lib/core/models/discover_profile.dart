import 'package:recordio/core/models/sample.dart';

class DiscoverProfile {
  String? id;
  String? fullname;
  String? email;
  String? bio;
  String? profileImageUrl;
  int? age;
  String? username;
  List<Sample>? samples;
  List<String> genres;

  DiscoverProfile({
    this.id,
    this.fullname,
    this.bio,
    this.profileImageUrl,
    this.age,
    this.email,
    this.username,
    this.samples,
    this.genres = const [],
  });

  factory DiscoverProfile.fromMap(dynamic map) {
    var temp;
    return DiscoverProfile(
        id: map['id']?.toString(),
        fullname: map['fullname']?.toString(),
        bio: map['bio']?.toString(),
        profileImageUrl: map['profileImageUrl']?.toString(),
        age: null == (temp = map['age']) ? null : (temp is num ? temp.toInt() : int.tryParse(temp)),
        email: map['email']?.toString(),
        samples: map['samples'] == null ? null : (map['samples'] as List).map((sample) => Sample.fromMap(sample)).toList(),
        username: map['username']?.toString(),
        genres: map['genres'] != null ? (map['genres'] as List).map((genre) => genre.toString()).toList() : []);
  }

  Map<String, dynamic> toMap() {
    return {
      'id': id,
      'fullname': fullname,
      'bio': bio,
      'profileImageUrl': profileImageUrl,
      'age': age,
      'email': email,
      'username': username,
    };
  }
}
