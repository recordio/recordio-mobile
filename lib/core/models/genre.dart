class Genre {
  String? id;
  String? name;

  Genre({
    this.id,
    this.name,
  });

  Map<String, dynamic> toMap() {
    return {
      'id': id,
      'name': name,
    };
  }

  factory Genre.fromMap(dynamic map) {
    return Genre(
      id: map['id']?.toString(),
      name: map['name']?.toString(),
    );
  }
}