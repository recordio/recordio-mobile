
class Media {
  String id;
  String filename;
  String mimeType;
  String url;
  String createdById;
  String type;
  int size;

  Media.fromJson(Map<String, dynamic> map)
      : id = map["id"],
        filename = map["filename"],
        mimeType = map["mimeType"],
        url = map["url"],
        createdById = map["createdById"],
        type = map["type"],
        size = map["size"];

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = id;
		data['filename'] = filename;
		data['mimeType'] = mimeType;
		data['url'] = url;
		data['createdById'] = createdById;
		data['type'] = type;
		data['size'] = size;
		return data;
	}
}
