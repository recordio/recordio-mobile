
class Message {
  String? id;
  String? conversationId;
  String? senderId;
  String? messageType;
  String? text;
  String? attachmentUrl;
  String? attachmentThumbUrl;
  DateTime? createdAt;
  DateTime? updatedAt;
  DateTime? localCreatedAt;

  Message({
    this.id,
    this.conversationId,
    this.senderId,
    this.messageType,
    this.text,
    this.attachmentUrl,
    this.attachmentThumbUrl,
    this.createdAt,
    this.updatedAt,
    this.localCreatedAt,
  });

  factory Message.fromMap(dynamic map) {
    var temp;
    return Message(
      id: map['id']?.toString(),
      conversationId: map['conversationId']?.toString(),
      senderId: map['senderId']?.toString(),
      messageType: map['messageType']?.toString(),
      text: map['text']?.toString(),
      attachmentUrl: map['attachmentUrl']?.toString(),
      attachmentThumbUrl: map['attachmentThumbUrl']?.toString(),
      createdAt: null == (temp = map['createdAt']) ? null : (temp is DateTime ? temp : DateTime.tryParse(temp)),
      updatedAt: null == (temp = map['updatedAt']) ? null : (temp is DateTime ? temp : DateTime.tryParse(temp)),
      localCreatedAt: null == (temp = map['localCreatedAt']) ? null : (temp is DateTime ? temp : DateTime.tryParse(temp)),
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'id': id,
      'conversationId': conversationId,
      'senderId': senderId,
      'messageType': messageType,
      'text': text,
      'attachmentUrl': attachmentUrl,
      'attachmentThumbUrl': attachmentThumbUrl,
      'createdAt': createdAt?.toString(),
      'updatedAt': updatedAt?.toString(),
      'localCreatedAt': localCreatedAt?.toString(),
    };
  }

  Message copyWith({
    String? id,
    String? conversationId,
    String? senderId,
    String? messageType,
    String? text,
    String? attachmentUrl,
    String? attachmentThumbUrl,
    DateTime? createdAt,
    DateTime? updatedAt,
    DateTime? localCreatedAt,
  }) {
    return Message(
      id: id ?? this.id,
      conversationId: conversationId ?? this.conversationId,
      senderId: senderId ?? this.senderId,
      messageType: messageType ?? this.messageType,
      text: text ?? this.text,
      attachmentUrl: attachmentUrl ?? this.attachmentUrl,
      attachmentThumbUrl: attachmentThumbUrl ?? this.attachmentThumbUrl,
      createdAt: createdAt ?? this.createdAt,
      updatedAt: updatedAt ?? this.updatedAt,
      localCreatedAt: localCreatedAt ?? this.localCreatedAt,
    );
  }
}
