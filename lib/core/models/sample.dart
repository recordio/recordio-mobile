class Sample {
  String? id;
  String? title;
  String? audioUrl;
  String? imageUrl;
  String? userId;
  String? createdAt;
  String? updatedAt;

  Sample({
    this.id,
    this.userId,
    this.title,
    this.audioUrl,
    this.imageUrl,
  });

  factory Sample.fromMap(dynamic map) {
    return Sample(
      id: map['id']?.toString(),
      userId: map['userId']?.toString(),
      title: map['title']?.toString(),
      audioUrl: map['audioUrl']?.toString(),
      imageUrl: map['imageUrl']?.toString(),
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'id': id,
      'userId': userId,
      'title': title,
      'audioUrl': audioUrl,
      'imageUrl': imageUrl,
    };
  }
}
