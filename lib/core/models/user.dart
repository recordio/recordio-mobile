class User {
  late String id;
  late String email;
  String? username;
  String? fullname;
  String? bio;
  String? profilePictureUrl;
  int? age;

  User({
    required this.id,
    required this.email,
    this.username,
    this.fullname,
    this.bio,
    this.profilePictureUrl,
    this.age,
  });

  User.fromMap(Map<String, dynamic> json) {
    id = json['id'];
    email = json['email'];
    username = json['username'];
    fullname = json['fullname'];
    bio = json['bio'];
    profilePictureUrl = json['profilePictureUrl'];
    age = json['age'];
  }

  Map<String, dynamic> toMap() {
    final map = <String, dynamic>{};
    map['id'] = id;
    map['email'] = email;
    map['username'] = username;
    map['fullname'] = fullname;
    map['bio'] = bio;
    map['profilePictureUrl'] = profilePictureUrl;
    map['age'] = age;
    return map;
  }

  User copyWith({
    String? id,
    String? email,
    String? username,
    String? fullname,
    String? bio,
    String? profilePictureUrl,
    int? age,
  }) {
    return User(
      id: id ?? this.id,
      email: email ?? this.email,
      username: username ?? this.username,
      fullname: fullname ?? this.fullname,
      bio: bio ?? this.bio,
      profilePictureUrl: profilePictureUrl ?? this.profilePictureUrl,
      age: age ?? this.age,
    );
  }
}
