
import 'package:recordio/core/models/user.dart';

class Vibe {
  String? id;
  String? message;
  String? type; // video, image, audio
  String? postedAt;
  User? author;
  String? url;
  int? likes;
  bool? liked;

  Vibe.initial();

  Vibe({
    this.id,
    this.message,
    this.type,
    this.postedAt,
    this.author,
    this.url,
    this.likes,
    this.liked,
  });

  factory Vibe.fromMap(dynamic map) {
    var temp;
    return Vibe(
      id: map['id']?.toString(),
      message: map['message']?.toString(),
      type: map['type']?.toString(),
      postedAt: map['postedAt']?.toString(),
      author: map['author'] != null ? User.fromMap(map['author']) : null,
      url: map['url']?.toString(),
      likes: null == (temp = map['likes']) ? null : (temp is num ? temp.toInt() : int.tryParse(temp)),
      liked: null == (temp = map['liked']) ? null : (temp is bool ? temp : (temp is num ? 0 != temp.toInt() : ('true' == temp.toString()))),
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'id': id,
      'message': message,
      'type': type,
      'postedAt': postedAt,
      'url': url,
      'likes': likes,
      'liked': liked,
    };
  }

  Vibe copyWith({
    String? id,
    String? message,
    String? type,
    String? postedAt,
    User? author,
    String? url,
    int? likes,
    bool? liked,
  }) {
    return Vibe(
      id: id ?? this.id,
      message: message ?? this.message,
      type: type ?? this.type,
      postedAt: postedAt ?? this.postedAt,
      author: author ?? this.author,
      url: url ?? this.url,
      likes: likes ?? this.likes,
      liked: liked ?? this.liked,
    );
  }
}
