import 'package:recordio/core/constants/constants.dart';
import 'package:recordio/core/models/conversation.dart';
import 'package:recordio/core/models/discover_profile.dart';
import 'package:recordio/core/services/services.dart';

class ConversationRepository {
  Future<Conversation?> requestConversation(String otherUserId) async {
    Map<String, dynamic>? data = await Services.api.post(Const.api.conversations + "/$otherUserId");
    return data != null ? Conversation.fromMap(data) : null;
  }

  Future<List<Conversation>?> getUserConversations() async {
    List<dynamic>? response = await Services.api.get(Const.api.userConversations) as List?;
    if (response == null) return null;
    return response.map((data) => Conversation.fromMap(data)).toList();
  }

  // Future<List<Message>> getChatMessages(String conversationId) async {
  //   final response = await Services.api.get(Const.api.messages + "/$conversationId") as List;
  //   return response.map((data) => Message.fromMap(data)).toList();
  // }
}
