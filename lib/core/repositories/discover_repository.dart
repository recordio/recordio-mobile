import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:recordio/core/constants/constants.dart';
import 'package:recordio/core/models/discover_profile.dart';
import 'package:recordio/core/models/sample.dart';
import 'package:recordio/core/services/services.dart';

class DiscoverRepository {
  final _store = FirebaseFirestore.instance;

  Future<List<DiscoverProfile>> fetch() async {
    List? result = await Services.api.get(Const.api.discoverProfiles) ;
    if (result == null) return [];

    return result.map((profile) => DiscoverProfile.fromMap(profile)).toList();
  }

  Future<Sample> sample(String id) async {
    QuerySnapshot query = await _store.collection('users').doc(id).collection('samples').get();
    return query.docs.map((documentSnapshot) => Sample.fromMap(documentSnapshot.data())).first;
  }
}
