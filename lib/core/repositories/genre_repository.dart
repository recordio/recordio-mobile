import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:recordio/core/models/genre.dart';
import 'package:recordio/core/services/services.dart';

class GenreRepository {

  final _store = FirebaseFirestore.instance;

  Future<List<Genre>> fetch() async {
    QuerySnapshot<Map<String, dynamic>> genres = await _store.collection('genres').get();
    return genres.docs.map((doc) => Genre(id: doc.id, name: doc.get('name') as String)).toList();
  }
}
