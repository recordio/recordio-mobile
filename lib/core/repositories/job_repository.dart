import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:recordio/core/models/User.dart';
import 'package:recordio/core/services/services.dart';

class UserRepository {

  Future<void> update(Map<String, dynamic> data) async {
    await Services.api.patch('/users', data);
  }
}
