import 'package:recordio/core/repositories/conversation_repository.dart';
import 'package:recordio/core/repositories/discover_repository.dart';
import 'package:recordio/core/repositories/genre_repository.dart';
import 'package:recordio/core/repositories/user_repository.dart';
import 'package:recordio/core/repositories/vibe_repository.dart';

class Repositories {
  static final users = UserRepository();
  static final genres = GenreRepository();
  static final DiscoverRepository discoverProfiles = DiscoverRepository();
  // static final ProfileRepository profiles = ProfileRepository();
  // static final JobsRepository jobs = JobsRepository();
  // static final GenresRepository genres = GenresRepository();
  static final ConversationRepository conversations = ConversationRepository();
  static final VibeRepository vibes = VibeRepository();
}