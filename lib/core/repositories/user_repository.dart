import 'dart:ffi';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:recordio/core/models/user.dart';
import 'package:recordio/core/models/genre.dart';
import 'package:recordio/core/models/sample.dart';
import 'package:recordio/core/services/services.dart';

class UserRepository {
  final _store = FirebaseFirestore.instance;

  Future<User?> update(Map<String, dynamic> data) async {
    Map<String, dynamic>? result = await Services.api.patch('/users', data);
    return result != null ? User.fromMap(result) : null;
  }

  Future<bool> isUsernameAvailable(String username) async {
    final result = await Services.api.get('/users/check/$username') as String;
    return result.toLowerCase() == 'true';
  }

  Future<User?> me() async {
    Map<String, dynamic>? result = await Services.api.get('/users/me');
    return result != null ? User.fromMap(result) : null;
  }

  Future<List<String>> fetchGenres() async {
    return [];
    // String? userId = Services.auth.user?.uid;
    // if (userId == null) return [];
    //
    // DocumentSnapshot<Map<String, dynamic>> documentSnapshot = await _store.collection('users').doc(userId).get();
    // final data = documentSnapshot.data()?['genres'] as List<dynamic>?;
    // return data != null ? data.map((id) => id.toString()).toList() : [];
  }

  Future<List<Sample>> fetchSamples() async {
    return [];

    // String? userId = Services.auth.user?.uid;
    // if (userId == null) return [];
    //
    // QuerySnapshot samplesSnapshot = await _store.collection('users/$userId/samples').get();
    // return samplesSnapshot.docs.map((doc) {
    //   Sample sample =  Sample.fromMap(doc);
    //   sample.id = doc.id;
    //   return sample;
    // }).toList();
  }

  Future<void> updateGenres(List<String> genres) async {
    // String? userId = Services.auth.user?.uid;
    // if (userId == null) return;
    //
    // await _store.collection('users').doc(userId).update({"genres": genres});
  }

  Future<void> updateSamples(List<Sample> samples) async {
    // String? userId = Services.auth.user?.uid;
    // if (userId == null) return;
    //
    // CollectionReference samplesCollection = _store.collection('users').doc(userId).collection("samples");
    // for (var sample in samples) {
    //   if (sample.id != null) {
    //     samplesCollection.doc(sample.id).update(sample.toMap());
    //   } else if (sample.audioUrl != null) {
    //     samplesCollection.add(sample.toMap());
    //   }
    // }
  }
}
