import 'package:recordio/core/constants/constants.dart';
import 'package:recordio/core/models/vibe.dart';
import 'package:recordio/core/services/services.dart';

class VibeRepository {
  Future<List<Vibe>> fetch() async {
    List<dynamic>? data = await Services.api.get(Const.api.vibes) as List?;
    return data != null ? data.map((vibe) => Vibe.fromMap(vibe)).toList() : const [];
  }

  Future<Vibe?> add(Vibe vibe) async {
    Map<String, dynamic>? data = await Services.api.post(Const.api.vibes, vibe.toMap());
    return data != null ? Vibe.fromMap(data) : null;
  }

  Future<Vibe> like(String vibeId) async {
    final data = await Services.api.post("${Const.api.vibes}/$vibeId/like");
    return Vibe.fromMap(data);
  }

  Future<Vibe> comment(dynamic vibe) async {
    final data = await Services.api.post(Const.api.vibes, vibe);
    return Vibe.fromMap(data);
  }
}