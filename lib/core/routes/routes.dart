import 'dart:io' show Platform;

import 'package:flutter/material.dart';
import 'package:recordio/core/models/conversation.dart';
import 'package:recordio/ui/chat/discussion_page.dart';
import 'package:recordio/ui/discover/discover_intro.dart';
import 'package:recordio/ui/discover/discover_page.dart';
import 'package:recordio/ui/dispatcher.dart';
import 'package:recordio/ui/home/home_page.dart';
import 'package:recordio/ui/login/signin_page.dart';
import 'package:recordio/ui/login/signup_page.dart';
import 'package:recordio/ui/onboarding/setup_music_preferences_page.dart';
import 'package:recordio/ui/onboarding/setup_profile_page.dart';
import 'package:recordio/ui/onboarding/setup_samples_page.dart';
import 'package:recordio/ui/onboarding/setup_summary.dart';
import 'package:recordio/ui/settings/settings_page.dart';
import 'package:recordio/ui/start_page.dart';
import 'package:recordio/ui/vibes/publish_vibe.dart';

enum PushDirection {
  up, left, down, right
}

class Routes {
  static const home = '/home';
  static const onboarding = '/onboarding';
  static const userSummary = '/user_summary';
  static const selectJobsPage = '/select_jobs_page';
  static const onboardingDiscovery = '/onboarding_discovery';
  static const onboardingProfilSetup = '/profile_setup';

  static const profileSetupSample = '/profile_setup/samples';
  static const profileSetupMusicPreferences = '/profile_setup/music_preferences';
  static const profileSetupSummary = '/profile_setup/summary';

  static const discoverIntro = '/discover/intro';
  static const discoverPage = '/discover/page';
  static const discoverFirstTime = '/discover/first_time';

  static const chatPage = '/chat';

  static const collaboratePage = '/collaborate/home';
  static const chatDiscussion = '/collaborate/chat/discussion';
  static const collaborationDiscussion = '/collaborate/collaboration/discussion';

  static const chooseImage = '/vibes/select/image';

  static const signIn = '/signin';
  static const signUp = '/signup';

  static const startPage = '/startup';

  static const videoView = '/video_view';
  static const videoPreview = '/video_preview';
  static const recordAudio = '/record_audio';



  static pushLeft(Widget page) {
    return PageRouteBuilder(
        pageBuilder: (context, animation, secondaryAnimation) => page,
        transitionDuration: const Duration(milliseconds: 500),
        transitionsBuilder: (context, animation, secondaryAnimation, child) {
          var begin = const Offset(1.0, 0.0);
          var end = Offset.zero;
          var curve = Curves.easeOutCubic;
          var tween = Tween(begin: begin, end: end).chain(CurveTween(curve: curve));
          var offsetAnimation = animation.drive(tween);

          return SlideTransition(
            position: offsetAnimation,
            child: child,
          );
        });
  }

  static PageRouteBuilder push(Widget page, { PushDirection? direction, Duration? duration }) {

    var begin = const Offset(1.0, 0.0);
    var end = Offset.zero;

    if (direction == PushDirection.up) {
      begin = const Offset(0.0, 1.0);
      end = Offset.zero;
    } else if (direction == PushDirection.right) {
      begin = const Offset(-1.0, 0.0);
      end = Offset.zero;
    } else if (direction == PushDirection.down) {
      begin = const Offset(0.0, -1.0);
      end = Offset.zero;
    }

    return PageRouteBuilder(
        pageBuilder: (context, animation, secondaryAnimation) => page,
        transitionDuration: duration ?? const Duration(milliseconds: 500),
        transitionsBuilder: (context, animation, secondaryAnimation, child) {
          var curve = Curves.easeOutExpo;
          var tween = Tween(begin: begin, end: end).chain(CurveTween(curve: curve));
          var offsetAnimation = animation.drive(tween);

          return SlideTransition(
            position: offsetAnimation,
            child: child,
          );
        });
  }

  static Route<dynamic> generateRoute(RouteSettings routeSettings) {
    switch (routeSettings.name) {
      case "/":
        return MaterialPageRoute(builder: (_) => const DispatcherPage());
      case StartPage.path:
        return MaterialPageRoute(builder: (_) => const StartPage());
      case HomePage.path:
        return MaterialPageRoute(builder: (_) => const HomePage());
      case SigninPage.path:
        return push(SigninPage(), direction: PushDirection.up, duration: const Duration(milliseconds: 1000));
      case SignupPage.path:
        return push(SignupPage(), direction: PushDirection.up, duration: const Duration(milliseconds: 1000));
      case SetupProfilePage.path:
        return pushLeft(const SetupProfilePage());
      case SetupMusicPreferencesPage.path:
        return pushLeft(const SetupMusicPreferencesPage());
      // case discoverIntro:
      //   return MaterialPageRoute(builder: (_) => DiscoverIntro());
      case SetupSamplesPage.path:
        return pushLeft(const SetupSamplesPage());
      case SetupSummaryPage.path:
        return pushLeft(const SetupSummaryPage());
      case DiscoverIntro.path:
        return pushLeft(const DiscoverIntro());
      case DiscoverPage.path:
        return pushLeft(const DiscoverPage());
      case SettingsPage.path:
        return pushLeft(const SettingsPage());
      case DiscussionPage.path:
        Conversation conversation = routeSettings.arguments as Conversation;
        return pushLeft(DiscussionPage(conversation));

      case PublishVibe.route:
        return push(const PublishVibe(), direction: PushDirection.up, duration: const Duration(milliseconds: 1000));
      // case profileSetupMusicPreferences:
      //   return MaterialPageRoute(builder: (_) => ProfileSetupMusicPreferences());
      // case profileSetupSummary:
      //   return MaterialPageRoute(builder: (_) => ProfileSetupSummary());
      // case discoverPage:
      //   return MaterialPageRoute(builder: (_) => DiscoveryPage());
      // case selectJobsPage:
      //   return MaterialPageRoute(builder: (_) => SelectJobs(), fullscreenDialog: true);
      //
      // // collaborate
      // case collaboratePage:
      //   return MaterialPageRoute(builder: (_) => CollaborateHome());
      // case chatDiscussion:
      //   return MaterialPageRoute(builder: (_) => Chat());
      // case chatPage:
      //   return PageRouteBuilder(
      //       pageBuilder: (context, animation, secondaryAnimation) => ChatPage(),
      //       transitionsBuilder: (context, animation, secondaryAnimation, child) {
      //         var begin = const Offset(1.0, 0.0);
      //         var end = Offset.zero;
      //         var curve = Curves.ease;
      //         var tween = Tween(begin: begin, end: end).chain(CurveTween(curve: curve));
      //         var offsetAnimation = animation.drive(tween);
      //
      //         return SlideTransition(
      //           position: offsetAnimation,
      //           child: child,
      //         );
      //       });
      // case collaborationDiscussion:
      //   return MaterialPageRoute(builder: (_) => CollaborationDiscussion());
      //
      // // vibes
      // case publishVibe:
      //   return MaterialPageRoute(builder: (_) => PublishVibe());
      // // case chooseImage:
      // //   return MaterialPageRoute(builder: (_) => ImageChooser());
      // case recordAudio:
      //   return MaterialPageRoute(builder: (_) => RecordVoice());
      //
      // // shared
      // case videoView:
      //   return MaterialPageRoute(builder: (_) => VideoCameraView());
      // case videoPreview:
      //   return MaterialPageRoute(builder: (_) => VideoPreview());

      // default
      default:
        return MaterialPageRoute(builder: (_) => const DispatcherPage());
    }
  }
}
