// import 'dart:convert';
// import 'dart:io';
//
// import 'package:dio/dio.dart';
// import 'package:flutter/material.dart';
// import 'package:fluttertoast/fluttertoast.dart';
// import 'package:recordio/core/constants/constants.dart';
// import 'package:recordio/core/services/services.dart';

import 'dart:io';
import 'package:flutter/material.dart';
import 'package:path/path.dart' as p;

import 'package:firebase_storage/firebase_storage.dart';
import 'package:recordio/core/services/http_service.dart';
import 'package:uuid/uuid.dart';

class Api {

  final _storage = FirebaseStorage.instance;
  final _uuid = const Uuid();

  get(String url) async {
    try {
      final response = await HttpService().dio.get(url);
      if (response.statusCode == 200) {
        return response.data;
      }
    } catch (e) {
      return null;
    }
  }

  Future<String?> upload(File file, [Function(TaskSnapshot)? onSendProgress]) async {
    String filename = _uuid.v4() + p.extension(file.path);
    try {
      UploadTask uploadTask = _storage.ref('uploads/$filename').putFile(file);
      uploadTask.snapshotEvents.listen(onSendProgress);
      TaskSnapshot taskSnapshot = await uploadTask;
      return taskSnapshot.ref.getDownloadURL();
    } on FirebaseException catch(e) {
      debugPrint(e.message);
      return null;
    }
  }


  post(String url, [dynamic body]) async {
    final response = await HttpService().dio.post(url, data: body);
    return response.data;
  }
//
  patch(String url, [dynamic body]) async {
    try {
      final response = await HttpService().dio.patch(url, data: body);
      if (response.statusCode == 200) {
        return response.data;
      }
      return null;
    } catch (e) {
      return null;
    }
  }
//
//   put() {}
//
//   delete() {}
//
//   uploadFile(String path, [Function(int, int)? onSendProgress, Function(int, int)? onReceiveProgress]) async {
//     FormData formData = FormData.fromMap({"file": await MultipartFile.fromFile(path)});
//     try {
//       final response = await NetworkService().dio.post(
//             "${Const.api.uploadFile}",
//             data: formData,
//             options: Options(sendTimeout: 60 * 1000 * 2, extra: {"file": path}),
//             onSendProgress: onSendProgress,
//             onReceiveProgress: onReceiveProgress,
//           );
//       if (response.statusCode == 200) {
//         return response.data;
//       } else if (response.statusCode == 500) {
//         throw Exception('Une erreur est survenue, veuillez reésayer plustard');
//       } else {
//         throw Exception(json.decode(response.data));
//       }
//     } on SocketException {
//       throw Exception("Pas de connection internet");
//     } on DioError catch (e) {
//       return throw _handleHttpError(e);
//     }
//   }
//
//   uploadFiles(String url, List<String> paths) async {
//     FormData formData = FormData.fromMap({"files": paths.map((path) async => await MultipartFile.fromFile(path))});
//     try {
//       final response = await NetworkService().dio.post("$url", data: formData);
//       if (response.statusCode == 200) {
//         return response.data;
//       } else if (response.statusCode == 500) {
//         throw Exception('Une erreur est survenue, veuillez reésayer plustard');
//       } else {
//         throw Exception(json.decode(response.data));
//       }
//     } on SocketException {
//       throw Exception("Pas de connection internet");
//     } on DioError catch (e) {
//       throw _handleHttpError(e);
//     }
//   }
//
//   _handleHttpError(DioError error) {
//     switch (error.type) {
//       case DioErrorType.receiveTimeout:
//         Fluttertoast.showToast(msg: "The request did not reach the server, maybe network is slow", backgroundColor: Colors.red);
//         break;
//       case DioErrorType.response:
//         final data = error.response?.data;
//         switch (data?["code"]) {
//           case 100:
//             Fluttertoast.showToast(msg: "Your session have expired", backgroundColor: Colors.red);
//             break;
//           case 101:
//             Fluttertoast.showToast(msg: "An error occurred on server", backgroundColor: Colors.red);
//             break;
//           default:
//             Fluttertoast.showToast(msg: data?["message"], backgroundColor: Colors.red);
//             break;
//         }
//         break;
//       default:
//         Fluttertoast.showToast(msg: json.encode(error.message), backgroundColor: Colors.red);
//         return error;
//     }
//     return error;
//   }
}
