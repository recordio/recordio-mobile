import 'package:audioplayers/audioplayers.dart';
import 'package:flutter/material.dart';

class AudioService {
  final _player = AudioPlayer();
  String _currentlyPlaying = "";

  Future<void> togglePlay(String url) async {
    if (_player.state == PlayerState.STOPPED) {
      await _player.play(url);
    } else if (_player.state == PlayerState.PAUSED) {
      _currentlyPlaying == url ? await _player.resume() :  await _player.play(url);
    } else if (_player.state == PlayerState.PLAYING){
      _currentlyPlaying == url ?await  _player.pause() : await _player.play(url);
    } else {
      await _player.play(url);
    }
    _currentlyPlaying = url;
  }

  stop() {

  }

  listenPlayerState(void Function(PlayerState, String) callback) {
    _player.onPlayerStateChanged.listen((state) {
      callback(state, _currentlyPlaying);
    });
  }
}