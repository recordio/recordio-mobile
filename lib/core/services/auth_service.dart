// import "dart:convert";
//
// import 'package:flutter/material.dart';
// import 'package:recordio/core/constants/constants.dart';
// import 'package:recordio/core/enums/onboarding_enum.dart';
// import 'package:recordio/core/models/models.dart';
// import 'package:recordio/core/routes/routes.dart';
// import 'package:recordio/core/services/services.dart';
// import "package:shared_preferences/shared_preferences.dart";
//
import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:recordio/core/services/services.dart';
import 'package:recordio/ui/login/signin_page.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../constants/constants.dart';
import '../models/auth.dart';

class AuthService {

  Future<void> saveAuth(Auth auth) async {
    final prefs = await SharedPreferences.getInstance();
    prefs.setString(Const.sharedPrefs.authKey, json.encode(auth.toMap()));
  }

  Future<void> removeAuth() async {
    final prefs = await SharedPreferences.getInstance();
    prefs.remove(Const.sharedPrefs.authKey);
  }

  Future<Auth?> get auth async {
    final prefs = await SharedPreferences.getInstance();
    final authString = prefs.getString(Const.sharedPrefs.authKey);
    if (authString == null) return null;
    return Auth.fromMap(json.decode(authString));
  }

  Future<void> signIn(String email, String password) async {
    Map<String, dynamic> result = await Services.api.post(Const.api.signIn, {"email": email, "password": password});
    Auth auth = Auth.fromMap(result);
    saveAuth(auth);
  }

//
//   Future<Auth?> refreshToken() async {
//     try {
//       Auth? auth = await getAuth;
//       final result = await NetworkService().tokenDio.get("${Const.api.refreshToken}/${auth?.refreshToken}");
//       auth = Auth.fromMap(result.data);
//       currentAuth = auth;
//       // print("renewed token : " + auth.accessToken);
//       return auth;
//     } catch (ex) {
//       print(ex);
//       Const.keys.navigatorKey.currentState!.pushNamedAndRemoveUntil(Routes.signIn, (route) => false);
//       return null;
//     }
//   }

  Future<void> signUp(String email, String password) async {
    Map<String, dynamic>? result = await Services.api.post(Const.api.signUp, {"email": email, "password": password});
    if (result == null) return;

    Auth auth = Auth.fromMap(result);
    await saveAuth(auth);
  }

  Future signOut(context) async {
    await removeAuth();
    Navigator.pushNamedAndRemoveUntil(context, SigninPage.path, (route) => false);
  }
}
