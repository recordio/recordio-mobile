import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';

class ErrorService {
  static final _instance = ErrorService._internal();

  ErrorService._internal();

  factory ErrorService() {
    return _instance;
  }

  show(String? message) {
    Fluttertoast.showToast(msg: message ?? "Une erreur est survenue", backgroundColor: Colors.red);
  }
}