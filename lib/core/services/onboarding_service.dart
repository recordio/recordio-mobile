import 'package:shared_preferences/shared_preferences.dart';

class OnboardingService {
  Future<bool> get isFinished async {
    final prefs = await SharedPreferences.getInstance();
    return prefs.getBool('finished_onboarding') ?? false;
  }

  set isFinished(Future<bool> finished) {
    SharedPreferences.getInstance().then((prefs) {
      return prefs.setBool('finished_onboarding', true);
    });
  }
}
