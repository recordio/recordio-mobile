import 'package:recordio/core/services/audio_service.dart';
import 'package:recordio/core/services/auth_service.dart';
import 'package:recordio/core/services/error_service.dart';
import 'package:recordio/core/services/onboarding_service.dart';
import 'package:recordio/core/services/work_service.dart';

import 'api_service.dart';

class Services {
  static final auth = AuthService();
  static final onboarding = OnboardingService();
  static final api = Api();
  static final work = WorkService.instance;
  static final audio = AudioService();
  static final error = ErrorService();
  // static final audioPlayer = AudioPlayerService();
  // static final conversation = ConversationService();
  // static final mqtt = MqttService.instance();
  // static final push = PushService.instance();
}