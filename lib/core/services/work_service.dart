import 'dart:async';

class WorkService {
  final _stream = StreamController<bool>();

  static WorkService? _instance;

  static WorkService get instance {
    if (_instance != null) {
      return _instance!;
    }
    _instance = WorkService();
    return _instance!;
  }

  Stream<bool> get stream => _stream.stream;

  start() {
    _stream.add(true);
  }

  stop() {
    _stream.add(false);
  }

  close() {
    _stream.close();
  }
}