import 'package:recordio/core/services/services.dart';

abstract class BaseState {
  bool _isLoading = false;
  bool _hasError = false;
  late Exception _exception;

  get isLoading => _isLoading;
  get hasError => _hasError;
  get exception => _exception;

  loading() {
    _isLoading = true;
  }

  error(String error) {
    _exception = Exception(error);
    _hasError = true;
    Services.error.show(error);
  }
}
