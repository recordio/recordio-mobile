import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

final navigationNotifierProvider = StateNotifierProvider<ColoredNavigationBarNotifier, NavigationBarStateModel>((ref) {
  return ColoredNavigationBarNotifier(null);
});

class NavigationBarStateModel {
  int currentIndex = 0;
  Color selectedColor = Colors.white;
  Color unSelectedColor = const Color(0xffDFA583);
  Color backgroundColor = const Color(0xffF2BA99);

  NavigationBarStateModel({
    required this.currentIndex,
    required this.selectedColor,
    required this.unSelectedColor,
    required this.backgroundColor,
  });

  NavigationBarStateModel.empty();

  NavigationBarStateModel copyWith({
    int? currentIndex,
    Color? selectedColor,
    Color? unSelectedColor,
    Color? backgroundColor,
  }) {
    return NavigationBarStateModel(
      currentIndex: currentIndex ?? this.currentIndex,
      selectedColor: selectedColor ?? this.selectedColor,
      unSelectedColor: unSelectedColor ?? this.unSelectedColor,
      backgroundColor: backgroundColor ?? this.backgroundColor,
    );
  }
}

class ColoredNavigationBarNotifier extends StateNotifier<NavigationBarStateModel> {
  final _unSelectedColorsList = [
    const Color(0xff129595),
    const Color(0xffDFA583),
    const Color(0xff129595),
    const Color(0xff5E59A3),
    const Color(0xff61338A),
  ];

  final _backgroundColorsList = [
    const Color(0xff14B5B5),
    const Color(0xffF2BA99),
    const Color(0xff61338A),
    const Color(0xff61338A),
    const Color(0xff007DA6),
  ];

  ColoredNavigationBarNotifier(NavigationBarStateModel? state)
      : super(state ??
      NavigationBarStateModel(
        currentIndex: 0,
        selectedColor: Colors.white,
        unSelectedColor: const Color(0xff129595),
        backgroundColor: const Color(0xff14B5B5),
      ));

  selectedItem(int index) {
    state = state.copyWith(currentIndex: index, unSelectedColor: _unSelectedColorsList[index], backgroundColor: _backgroundColorsList[index]);
  }
}