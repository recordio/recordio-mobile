import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:recordio/core/services/services.dart';
import 'package:recordio/ui/home/home_page.dart';
import 'package:recordio/ui/onboarding/setup_profile_page.dart';
import 'package:recordio/ui/start_page.dart';

final authFuture = FutureProvider<bool>((ref) {
  return Future.value(false);
  // return Services.auth.isUserAuthenticated;
});

final startupPageFutureProvider = FutureProvider<String>((ref) async {
  if (await Services.auth.auth != null) {
    final bool? isFinished = await Services.onboarding.isFinished;
    return isFinished != null && !isFinished ? SetupProfilePage.path : HomePage.path;
  } else {
    return StartPage.path;
  }
});