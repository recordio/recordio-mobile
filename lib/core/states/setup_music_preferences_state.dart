import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:reactive_forms/reactive_forms.dart';
import 'package:recordio/core/models/genre.dart';
import 'package:recordio/core/repositories/repositories.dart';
import 'package:recordio/core/services/services.dart';

final musicPreferencesProvider = StateNotifierProvider<MusicPreferencesNotifier, List<MusicPreferenceState>>((ref) {
  return MusicPreferencesNotifier([], ref);
});

class MusicPreferenceState {
  Genre genre;
  bool selected;

  MusicPreferenceState({
    required this.genre,
    required this.selected,
  });
}

class MusicPreferencesNotifier extends StateNotifier<List<MusicPreferenceState>> {
  Ref ref;

  MusicPreferencesNotifier(List<MusicPreferenceState> state, this.ref) : super(state) {
    _loadGenres();
  }

  List<Genre> selected() {
    return state.fold<List<Genre>>([], (acc, state) => state.selected ? [state.genre, ...acc] : acc).toList();
  }

  _loadGenres() async {
    List<String> genresSelected = await Repositories.users.fetchGenres();
    List<Genre> genres = await Repositories.genres.fetch();
    state = genres.map((genre) {
      return genresSelected.contains(genre.id)
          ? MusicPreferenceState(genre: genre, selected: true)
          : MusicPreferenceState(genre: genre, selected: false);
    }).toList();
  }

  select(MusicPreferenceState musicPreference) {
    state = state.map((pref) {
      if (pref.genre.id == musicPreference.genre.id) {
        musicPreference.selected = !pref.selected;
        return musicPreference;
      }
      return pref;
    }).toList();
  }

  saveGenreSelected() async {
    List<String> genresSelected = state.fold([], (acc, cur) => cur.selected ? [cur.genre.id ?? "", ...acc] : acc);
    await Repositories.users.updateGenres(genresSelected);
  }
}
