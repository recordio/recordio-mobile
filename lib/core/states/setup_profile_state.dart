import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:reactive_forms/reactive_forms.dart';
import 'package:recordio/core/models/user.dart';
import 'package:recordio/core/repositories/repositories.dart';
import 'package:recordio/core/services/services.dart';
import 'package:recordio/core/states/base_state.dart';

final setupProfileProvider = StateNotifierProvider<SetupProfileNotifier, SetupProfileState>((ref) {
  return SetupProfileNotifier(SetupProfileState()..loading(), ref);
});

final setupProfileFormProvider = Provider<FormGroup>((_) {
  return fb.group({
    'username': [Validators.required],
    'fullname': [Validators.required],
    'bio': [],
  });
});

class SetupProfileState extends BaseState {
  User? user;
  String? localProfilePath;
  bool usernameSealed;

  factory SetupProfileState.fromMap(dynamic map) {
    return SetupProfileState(
      usernameSealed: map['usernameSealed'] ?? false,
      localProfilePath: map['localProfilePath']?.toString(),
    );
  }

  SetupProfileState({
    this.user,
    this.usernameSealed = false,
    this.localProfilePath,
  });

  SetupProfileState copyWith({
    User? user,
    bool? usernameSealed,
    String? localProfilePath,
  }) {
    return SetupProfileState(
      user: user ?? this.user,
      usernameSealed: usernameSealed ?? this.usernameSealed,
      localProfilePath: localProfilePath ?? this.localProfilePath,
    );
  }
}

class SetupProfileNotifier extends StateNotifier<SetupProfileState> {
  Ref ref;

  SetupProfileNotifier(SetupProfileState state, this.ref) : super(state) {
    _loadProfile();
  }

  profilePicture(String imagePath) async {
    String? imageUrl = await Services.api.upload(File(imagePath));
    User? user = state.user?.copyWith(profilePictureUrl: imageUrl);
    if (user != null) {
      await Repositories.users.update(user.toMap());
      state = state.copyWith(localProfilePath: imagePath, user: user);
    }
  }

  Future<bool> isUsernameAvailable() async {
    String? username = ref.read(setupProfileFormProvider).value["username"] as String?;
    if (username == null) return false;

    return await Repositories.users.isUsernameAvailable(username);
  }

  Future<void> save() async {
    Map<String, Object?>? formData = ref.read(setupProfileFormProvider).value;
    if (state.user != null) {
      try {
        User? user = await Repositories.users.update(formData);
        state = state.copyWith(user: user);
      } catch(_) {
        return;
      }
    }
  }

  Future<void> _loadProfile() async {
    User? user = await Repositories.users.me();
    if (user == null) {
      state.error('Error fetching user');
      return;
    }

    final form = ref.watch(setupProfileFormProvider);
    form.patchValue(user.toMap());
    state = user.username != null ? state.copyWith(user: user, usernameSealed: true) : state.copyWith(user: user);
  }
}
