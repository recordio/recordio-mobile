import 'dart:io';

import 'package:audioplayers/audioplayers.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:image_picker/image_picker.dart';
import 'package:recordio/core/models/sample.dart';
import 'package:recordio/core/repositories/repositories.dart';
import 'package:collection/collection.dart';

final samplesProvider = StateNotifierProvider<SampleNotifier, List<SampleState>>((ref) {
  return SampleNotifier(List.generate(3, (index) => SampleState(id: index, sample: Sample())), ref);
});

class SampleState {
  int id;
  Sample sample;
  XFile? image;
  String? title;
  File? audio;
  String? audioUrl;
  bool? isNew = false;
  bool? isPlaying = false;
  String? imageUrl;
  String? mediaId;

  SampleState({
    required this.id,
    required this.sample,
  });
}

class SampleNotifier extends StateNotifier<List<SampleState>> {
  Ref ref;
  final player = AudioPlayer();

  SampleNotifier(List<SampleState> state, this.ref) : super(state) {
    _fetchSamples();
  }

  _fetchSamples() async {
    List<Sample> samples = await Repositories.users.fetchSamples();
    if (samples.isNotEmpty) {
      state = samples.mapIndexed<SampleState>((index, sample) => SampleState(id: index, sample: sample)).toList();
      List<SampleState> emptySampleStates = [];
      if (state.length < 3) {
        for(var i = 0; i < 3 - state.length; i++ ) {
          emptySampleStates.add(SampleState(id: i + state.length, sample: Sample()));
        }
        state = [...state, ...emptySampleStates];
      }
    }
  }

  playSample(SampleState sampleState) {
    if (sampleState.audio?.path == null && sampleState.sample.audioUrl == null) return;

    for (var stateModel in state) {
      stateModel.isPlaying = stateModel.id == sampleState.id ? true : false;
    }

    state = [...state];

    player.play(sampleState.audio?.path ?? sampleState.sample.audioUrl ?? "", stayAwake: true);
  }

  stopSample(SampleState sampleState) {
    for (var stateModel in state) {
      stateModel.isPlaying = false;
    }
    state = [...state];
    player.stop();
  }

  update(SampleState sampleState) {
    state = state.map((s) => s == sampleState ? sampleState : s).toList();
  }

  sendSamples() async {
    List<Sample> samples = state.map((s) => Sample(
      audioUrl: s.audioUrl,
      title: s.title,
      imageUrl: s.imageUrl,
    )).toList();

    await Repositories.users.updateSamples(samples);
  }

}
