import 'dart:io';

import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:recordio/core/enums/onboarding_enum.dart';
import 'package:recordio/core/services/services.dart';

final uploadNotifier = ChangeNotifierProvider((_) => UploadNotifier());

class UploadNotifier extends ChangeNotifier {
  double uploadRatio = -1.0;
  var progression = Progression.uninitialized;
  File? lastFile;
  // Media? mediaUploaded;

  Future<String?> upload(File file) async {
    progression = Progression.uninitialized;
    try {
      lastFile = file;
      final url = await Services.api.upload(file, onSendProgress);
      _finished();
      return url;
    } catch (e) {
      progression = Progression.error;
      notifyListeners();
      return null;
    }
  }

  retry() async {
    // mediaUploaded = null;
    if (lastFile != null) await upload(lastFile!);
  }

  onSendProgress(TaskSnapshot task) {
    int count = task.bytesTransferred;
    int total = task.totalBytes;
    uploadRatio = count / total;
    if (uploadRatio < 0) {
      progression = Progression.uninitialized;
    } else if (uploadRatio >= 0 && uploadRatio <= 1) {
      progression = Progression.uploading;
    }
    notifyListeners();
  }

  _finished() {
    progression = Progression.finished;
    notifyListeners();
  }
}
