
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:recordio/core/services/services.dart';

final workProvider = StreamProvider<bool>((ref) => Services.work.stream);
