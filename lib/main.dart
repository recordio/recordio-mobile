import 'package:firebase_app_check/firebase_app_check.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';


import 'core/constants/constants.dart';
import 'core/routes/routes.dart';

Future<void> main() async {
  // smaple comment
  // FirebaseMessaging.onBackgroundMessage(PushService.backgroundMessageHandler);
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  await FirebaseAppCheck.instance.activate();
  runApp(const ProviderScope(child: MyApp()));
  SystemChrome.setSystemUIOverlayStyle(const SystemUiOverlayStyle(
    statusBarColor: Colors.transparent,
  ));
}

class MyApp extends StatefulWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  final Future<FirebaseApp> _initialization = Firebase.initializeApp();

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
      future: _initialization,
      builder: (BuildContext context, AsyncSnapshot<FirebaseApp> snapshot) {
        if (snapshot.hasError) {
          return Container();
        }

        if (snapshot.connectionState == ConnectionState.done) {
          return MaterialApp(
            navigatorKey: Const.keys.navigatorKey,
            debugShowCheckedModeBanner: false,
            title: 'Recordio',
            theme: ThemeData(
              fontFamily: 'raleway',
              primaryColor: const Color(0xff61338A),
              accentColor: const Color(0xffF2BA99),
              visualDensity: VisualDensity.adaptivePlatformDensity,
              inputDecorationTheme: const InputDecorationTheme(
                labelStyle: TextStyle(color: Color(0xff61338A)),
                enabledBorder: UnderlineInputBorder(
                  borderSide: BorderSide(color: Color(0xff61338A)),
                ),
              ),
            ),
            initialRoute: "/",
            onGenerateRoute: Routes.generateRoute,
          );
        }

        // Otherwise, show something whilst waiting for initialization to complete
        return const Center(child: CircularProgressIndicator());
      },
    );
  }
}
