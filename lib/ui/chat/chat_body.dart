import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:intl/intl.dart';
import 'package:recordio/core/models/auth.dart';
import 'package:recordio/core/models/conversation.dart';
import 'package:recordio/core/models/message.dart';
import 'package:recordio/core/services/services.dart';
import 'package:recordio/ui/chat/providers/conversation_providers.dart';
import 'package:recordio/ui/chat/states/conversation_state.dart';

class ChatBody extends ConsumerWidget {
  final Conversation conversation;
  final ScrollController _scrollController;

  const ChatBody(this.conversation, this._scrollController, {Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    return FutureBuilder(
        future: Services.auth.auth,
        builder: (context, AsyncSnapshot<Auth?> snapshot) {
          if (snapshot.connectionState == ConnectionState.done && snapshot.hasData) {
            final String? currentUserId = snapshot.data?.id;

            if (currentUserId == null) {
              // todo redirect to signin page
              return Container();
            }

            ConversationState conversationState = ref.watch(conversationProvider(conversation.id));

            return ListView.separated(
              padding: const EdgeInsets.all(0),
              physics: const BouncingScrollPhysics(),
              shrinkWrap: true,
              reverse: true,
              controller: _scrollController,
              itemCount: conversationState.messages.length,
              separatorBuilder: (context, index) {
                return const SizedBox(height: 16);
              },
              itemBuilder: (context, index) {
                final message = conversationState.messages.elementAt(index);
                return Builder(
                  builder: (context) {
                    if (message.senderId == currentUserId) {
                      // my message
                      return Align(
                        alignment: Alignment.centerRight,
                        child: Container(
                          constraints: const BoxConstraints(maxWidth: 260),
                          padding: const EdgeInsets.only(top: 24, bottom: 24, right: 27, left: 50),
                          decoration: BoxDecoration(
                            color: const Color(0xff14B5B5).withOpacity(.10),
                            borderRadius: const BorderRadius.only(
                              topLeft: Radius.circular(20),
                              bottomLeft: Radius.circular(20),
                              topRight: Radius.circular(20),
                              bottomRight: Radius.circular(2),
                            ),
                          ),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.end,
                            children: [
                              MessageContent(message),
                              const SizedBox(height: 6),
                              Row(
                                mainAxisAlignment: MainAxisAlignment.end,
                                children: const [
                                  Text(
                                    "20:33",
                                    style: TextStyle(
                                      color: Color(0xff14B5B5),
                                      fontSize: 9,
                                      fontWeight: FontWeight.w300,
                                    ),
                                  ),
                                ],
                              ),
                            ],
                          ),
                        ),
                      );
                    } else {
                      // other user message
                      return Align(
                        alignment: Alignment.centerLeft,
                        child: Container(
                          constraints: const BoxConstraints(maxWidth: 260),
                          padding: const EdgeInsets.only(top: 24, bottom: 24, right: 50, left: 27),
                          decoration: BoxDecoration(
                              color: const Color(0xffE5E5E5).withOpacity(.30),
                              borderRadius: const BorderRadius.only(
                                  topLeft: Radius.circular(20),
                                  bottomLeft: Radius.circular(2),
                                  topRight: Radius.circular(20),
                                  bottomRight: Radius.circular(20))),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              MessageContent(message),
                              const SizedBox(height: 6),
                              Row(
                                mainAxisAlignment: MainAxisAlignment.start,
                                children: [
                                  Text(
                                    parseDate(message),
                                    style: const TextStyle(
                                      color: Color(0xff14B5B5),
                                      fontSize: 9,
                                      fontWeight: FontWeight.w300,
                                    ),
                                  ),
                                ],
                              ),
                            ],
                          ),
                        ),
                      );
                    }
                  },
                );
              },
            );
          } else if (snapshot.hasError) {
            return Center(
              child: ElevatedButton(
                onPressed: () {},
                child: const Text("Réessayer"),
              ),
            );
          } else {
            return const Center(
              child: CircularProgressIndicator(),
            );
          }
        });
  }

  String parseDate(Message message) {
    final formatter = DateFormat('HH:mm');
    if (message.createdAt != null) {
      return formatter.format(message.createdAt!);
    } else if (message.localCreatedAt != null) {
      return formatter.format(message.localCreatedAt!);
    }
    return "";
  }
}

class MessageContent extends StatelessWidget {
  const MessageContent(
    this.message, {
    Key? key,
  }) : super(key: key);

  final Message message;

  @override
  Widget build(BuildContext context) {
    return Text(message.text ?? "");
  }
}
