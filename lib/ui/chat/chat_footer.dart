import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:recordio/core/models/conversation.dart';
import 'package:recordio/core/repositories/repositories.dart';
import 'package:recordio/ui/chat/providers/conversation_providers.dart';
import 'package:recordio/ui/chat/states/conversation_state.dart';

class ChatFooter extends ConsumerWidget {

  final Conversation _conversation;

  const ChatFooter(this._conversation, {Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    // final conversationSelected = useProvider(chatSelected).state;
    // final conversationItemState = useProvider(conversationsStateProvider).items.elementAt(conversationSelected);
    final _textEditingController = TextEditingController();

    return ConstrainedBox(
      constraints: BoxConstraints(
        maxWidth: MediaQuery.of(context).size.width,
      ),
      child: Container(
        height: 50,
        decoration: BoxDecoration(color: Colors.white, borderRadius: const BorderRadius.all(Radius.circular(20)), boxShadow: [
          BoxShadow(
            blurRadius: 4,
            offset: const Offset(0, 4),
            color: Colors.black.withOpacity(.05),
          ),
        ]),
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 12, vertical: 5),
          child: Row(
            mainAxisSize: MainAxisSize.min,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Expanded(
                child: TextFormField(
                    style: const TextStyle(fontSize: 14),
                    decoration: const InputDecoration(border: InputBorder.none, enabledBorder: InputBorder.none),
                    controller: _textEditingController),
              ),
              IconButton(
                icon: const Icon(Icons.send, size: 22),
                onPressed: () async {
                  final text = _textEditingController.text;
                  await ref.read(conversationProvider(_conversation.id).notifier).sendMessage(text);
                  _textEditingController.text = '';
                },
              ),
            ],
          ),
        ),
      ),
    );
  }
}