import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:recordio/core/models/conversation.dart';

class ChatHeader extends StatelessWidget {
  final Conversation conversation;

  const ChatHeader(
    this.conversation, {
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 80,
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: const BorderRadius.all(
          Radius.circular(20),
        ),
        boxShadow: [
          BoxShadow(
            offset: const Offset(0, 4),
            blurRadius: 4,
            color: Colors.black.withOpacity(.25),
          ),
        ],
      ),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          const BackButton(),
          CircleAvatar(
            radius: 20,
            backgroundImage: conversation.imageUrl != null
                ? CachedNetworkImageProvider(conversation.imageUrl!)
                : const AssetImage("assets/images/background_square.png") as ImageProvider,
          ),
          Padding(
            padding: const EdgeInsets.only(left: 30),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  conversation.title ?? "",
                  style: const TextStyle(
                    fontSize: 14,
                    fontWeight: FontWeight.w700,
                    color: Colors.black,
                  ),
                ),
                const Text(
                  "Online",
                  style: TextStyle(
                    fontSize: 9,
                    fontWeight: FontWeight.w200,
                    color: Colors.black,
                  ),
                ),
              ],
            ),
          ),
          Expanded(
            child: Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                FlatButton(
                  child: const Icon(Icons.more_horiz_rounded, size: 30),
                  onPressed: () {},
                ),
              ],
            ),
          )
        ],
      ),
    );
  }
}
