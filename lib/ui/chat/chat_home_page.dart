import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:recordio/core/constants/constants.dart';
import 'package:recordio/core/models/conversation.dart';
import 'package:recordio/ui/chat/discussion_page.dart';
import 'package:recordio/ui/chat/providers/conversation_list_providers.dart';
import 'package:recordio/ui/chat/states/conversation_list_state.dart';

class ChatHomePage extends ConsumerWidget {
  static const path = "/chat";

  const ChatHomePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    ConversationListState conversationListState = ref.watch(conversationListProvider);

    if (conversationListState.isLoading) {
      return const Center(child: CircularProgressIndicator());
    }

    if (conversationListState.hasError) {
      return Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            const Text("Une erreur est survenue"),
            ElevatedButton(
              child: const Text("Reésayer"),
              onPressed: () {
                ref.read(conversationListProvider.notifier).fetchConversations();
              },
            ),
          ],
        ),
      );
    }

    return Scaffold(
      appBar: AppBar(
        iconTheme: IconTheme.of(context).copyWith(color: Colors.black),
        backgroundColor: Colors.transparent,
        elevation: 0,
        title: const Text(
          "Chat",
          style: TextStyle(
            fontSize: 36,
            color: Colors.black87,
            fontWeight: FontWeight.bold,
          ),
        ),
      ),
      body: Container(
        padding: const EdgeInsets.symmetric(vertical: 20.0),
        child: ListView.separated(
          itemBuilder: (context, index) {
            Conversation conversation = conversationListState.conversations.elementAt(index);
            // Services.mqtt.subscribe("${Const.mqtt.conversationTopic}/${conversation$.conversation.id}");
            // Services.push.subscribeToTopic(conversation$.conversation);
            return ListTile(
              leading: CircleAvatar(
                radius: 25,
                backgroundImage: conversation.imageUrl != null
                    ? CachedNetworkImageProvider(conversation.imageUrl!)
                    : const AssetImage("assets/images/background_square.png") as ImageProvider,
              ),
              title: Text(conversation.title ?? "New user"),
              trailing: Wrap(
                direction: Axis.vertical,
                crossAxisAlignment: WrapCrossAlignment.end,
                children: [
                  const Text("01/20/21", style: TextStyle(fontSize: 10)),
                  Padding(
                    padding: const EdgeInsets.only(top: 2),
                    child: Container(
                      height: 18,
                      width: 18,
                      decoration: BoxDecoration(
                        borderRadius: const BorderRadius.all(Radius.circular(20)),
                        color: Const.color.collaborate,
                      ),
                      child: const Center(
                        child: Text(
                          // "${conversation$.unReadMessages}",
                          "2",
                          style: TextStyle(fontSize: 10, color: Colors.white),
                        ),
                      ),
                    ),
                  ),
                ],
              ),
              onTap: () {
                Navigator.pushNamed(context, DiscussionPage.path, arguments: conversation);
              },
            );
          },
          separatorBuilder: (context, index) {
            return const Divider();
          },
          itemCount: conversationListState.conversations.length,
        ),
      ),
    );
  }
}
