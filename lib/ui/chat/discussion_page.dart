import 'dart:math';

import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:recordio/core/models/conversation.dart';
import 'package:recordio/ui/chat/chat_body.dart';
import 'package:recordio/ui/chat/chat_footer.dart';
import 'package:recordio/ui/chat/chat_header.dart';

class DiscussionPage extends StatelessWidget {

  final Conversation conversation;
  final ScrollController _scrollController = ScrollController();

  static const path = '/chat/conversation';

  DiscussionPage(this.conversation, {Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {

    return GestureDetector(
      onTap: () => FocusScope.of(context).requestFocus(FocusNode()),
      child: SafeArea(
        top: false,
        child: Scaffold(
          body: Padding(
            padding: const EdgeInsets.only(top: 36, left: 20, right: 20),
            child: Column(
              children: [
                ChatHeader(conversation),
                Expanded(child: ChatBody(conversation, _scrollController)),
                const SizedBox(height: 10),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: ChatFooter(conversation),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}




