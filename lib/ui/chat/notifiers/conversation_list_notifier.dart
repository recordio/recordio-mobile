import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:recordio/core/models/conversation.dart';
import 'package:recordio/core/models/discover_profile.dart';
import 'package:recordio/core/models/message.dart';
import 'package:recordio/core/repositories/repositories.dart';
import 'package:recordio/core/services/services.dart';
import 'package:recordio/ui/chat/providers/conversation_providers.dart';
import 'package:recordio/ui/chat/states/conversation_list_state.dart';

class ConversationListNotifier extends StateNotifier<ConversationListState> {
  final Ref ref;

  ConversationListNotifier(this.ref, [ConversationListState? state])
      : super(state ?? ConversationListState(conversations: [])
          ..loading()) {
    fetchConversations();
  }

  Future<void> fetchConversations() async {
    List<Conversation>? conversations = await Repositories.conversations.getUserConversations();
    if (conversations == null) {
      state = state.copyWith()..error("Error getting conversations");
      return;
    }

    state = state.copyWith(conversations: conversations);
  }

  Future<Conversation?> requestConversation(DiscoverProfile profile) async {
    if (profile.id == null) return null;

    Conversation? conversation = await Repositories.conversations.requestConversation(profile.id!);
    if (conversation == null) return null;

    state = state.copyWith(conversations: [conversation, ...state.conversations]);



    return conversation;
  }

  isProfileAdded(DiscoverProfile profile) {
    // return state.items.any((contact) => contact.conversation.otherId == profile.id);
  }
}
