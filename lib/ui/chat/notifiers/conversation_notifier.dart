import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:recordio/core/enums/chat_enum.dart';
import 'package:recordio/core/models/message.dart';
import 'package:recordio/core/services/services.dart';
import 'package:recordio/ui/chat/states/conversation_state.dart';

class ConversationNotifier extends StateNotifier<ConversationState> {
  final String conversationId;
  final Ref ref;
  final db = FirebaseFirestore.instance;

  ConversationNotifier(this.conversationId, this.ref) : super(ConversationState(messages: [])..loading()) {}

  Future<void> sendMessage(String text) async {
    final Message message = Message(
      conversationId: conversationId,
      senderId: (await Services.auth.auth)?.id,
      text: text,
      messageType: MessageType.text.value,
      localCreatedAt: DateTime.now(),
    );
    db.collection('conversations/$conversationId/messages').add(message.toMap());
  }

  addMessage(Message message) {
    state = state.copyWith(messages: [message, ...state.messages]);
  }
}
