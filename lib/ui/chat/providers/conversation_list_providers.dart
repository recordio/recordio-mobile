import 'dart:async';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:recordio/core/models/conversation.dart';
import 'package:recordio/core/models/message.dart';
import 'package:recordio/ui/chat/notifiers/conversation_list_notifier.dart';
import 'package:recordio/ui/chat/providers/conversation_providers.dart';
import 'package:recordio/ui/chat/states/conversation_list_state.dart';

final conversationListProvider = StateNotifierProvider<ConversationListNotifier, ConversationListState>((ref) => ConversationListNotifier(ref));

final initChatProvider = Provider((ref) {
  final db = FirebaseFirestore.instance;
  List<StreamSubscription>? collectionSubscriptions;

  ref.listen(conversationListProvider, (ConversationListState? previous, ConversationListState next) {
    List<Conversation> conversations = next.conversations;
    collectionSubscriptions?.forEach((subscription) => subscription.cancel());

    for (var conversation in conversations) {
      StreamSubscription subscription = db.collection('conversations/${conversation.id}/messages').snapshots().listen((QuerySnapshot event) {
        for (DocumentChange change in event.docChanges) {
          dynamic data = change.doc.data();
          Message message = Message.fromMap(data);
          message.id = change.doc.id;
          if (message.conversationId == null) continue;
          ref.read(conversationProvider(conversation.id).notifier).addMessage(message);
        }
      });
      collectionSubscriptions?.add(subscription);
    }
  });
});
