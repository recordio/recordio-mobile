import 'dart:async';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:recordio/core/models/conversation.dart';
import 'package:recordio/ui/chat/notifiers/conversation_list_notifier.dart';
import 'package:recordio/ui/chat/notifiers/conversation_notifier.dart';
import 'package:recordio/ui/chat/states/conversation_list_state.dart';
import 'package:recordio/ui/chat/states/conversation_state.dart';

final conversationProvider = StateNotifierProvider.family<ConversationNotifier, ConversationState, String>(
  (ref, String conversationId) => ConversationNotifier(conversationId, ref),
);
