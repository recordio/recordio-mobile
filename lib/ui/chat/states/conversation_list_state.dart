// here we are trying to use class list directly without going
// through item state

import 'package:recordio/core/models/conversation.dart';
import 'package:recordio/core/states/base_state.dart';

class ConversationListState with BaseState {
  final List<Conversation> conversations;

  ConversationListState({required this.conversations});

  ConversationListState copyWith({ List<Conversation>? conversations }) {
    return ConversationListState(conversations: conversations ?? this.conversations);
  }
}