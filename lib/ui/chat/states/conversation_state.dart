import 'package:recordio/core/models/message.dart';
import 'package:recordio/core/states/base_state.dart';

class ConversationState extends BaseState {
  List<Message> messages = const [];

  ConversationState({
    required this.messages,
  });

  ConversationState copyWith({List<Message>? messages}) {
    return ConversationState(
      messages: messages ?? this.messages,
    );
  }
}
