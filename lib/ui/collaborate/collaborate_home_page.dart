import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:recordio/core/constants/constants.dart';
import 'package:recordio/core/routes/routes.dart';

class CollaboratePage extends ConsumerWidget {
  const CollaboratePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    // final tabBarController = useTabController(initialLength: 2);
    return Scaffold(
      appBar: AppBar(
        title: const Text("Collaboration"),
        brightness: Brightness.dark,
        backgroundColor: Const.color.collaborate,
        actions: [
          Padding(
            padding: const EdgeInsets.only(right: 10.0),
            child: IconButton(
                onPressed: () {
                  Navigator.pushNamed(context, Routes.chatPage);
                },
                icon: Icon(Icons.send)),
          ),
        ],
      ),
      body: SafeArea(
        top: true,
        child: Container(),
      ),
    );
  }
}
