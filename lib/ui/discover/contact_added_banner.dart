// import 'package:flutter/material.dart';
// import 'package:flutter_hooks/flutter_hooks.dart';
// import 'package:flutter_screenutil/flutter_screenutil.dart';
// import 'package:hooks_riverpod/hooks_riverpod.dart';
// import 'package:recordio/core/notifiers/discover.provider.dart';
//
// class ContactAddedBanner extends HookWidget {
//   @override
//   Widget build(BuildContext context) {
//     final c = useContext();
//
//     Future.delayed(Duration(seconds: 1), () {
//       c.read(displayBanner).state = false;
//       return;
//     });
//
//     return Container(
//       decoration: BoxDecoration(
//         color: Colors.black.withOpacity(.4),
//       ),
//       child: Center(
//         child: Container(
//           decoration: BoxDecoration(color: Colors.pink, borderRadius: BorderRadius.circular(7)),
//           height: .6.sh,
//           width: .8.sw,
//           child: Column(
//             children: [
//               Image(image: AssetImage("assets/images/celebrating.png")),
//               Text("Félicitations, xxxx est fait parti de votre réseau"),
//             ],
//           ),
//         ),
//       ),
//     );
//   }
// }
