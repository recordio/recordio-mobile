import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:recordio/core/constants/constants.dart';
import 'package:recordio/ui/discover/providers/discover_list_providers.dart';
import 'package:recordio/ui/discover/states/discover_list_state.dart';

import 'discover_page.dart';

class DiscoverIntro extends ConsumerStatefulWidget {

  static const path = "/discover/intro";

  const DiscoverIntro({Key? key}) : super(key: key);

  @override
  ConsumerState<DiscoverIntro> createState() => _DiscoverIntroState();
}

class _DiscoverIntroState extends ConsumerState<DiscoverIntro> {

  @override
  void initState() {
    super.initState();
    Future.delayed(const Duration(seconds: 3), () {
      ref.read(isFirstTimeStateProvider.notifier).state = true;
      Navigator.pushNamedAndRemoveUntil(context, DiscoverPage.path, (route) => false);
    });
  }


  @override
  Widget build(BuildContext context) {

    return Scaffold(
        appBar: AppBar(
          elevation: 0,
          backgroundColor: Colors.white,
          iconTheme: const IconThemeData(color: Colors.black),
        ),
        body: Container(
          color: Colors.white,
          width: MediaQuery.of(context).size.width,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              SizedBox(height: 91 - Const.common.headerSize),
              TweenAnimationBuilder(
                tween: Tween<double>(begin: 0, end: 1),
                duration: const Duration(seconds: 2),
                curve: Curves.easeOutExpo,
                builder: (context, double size, _) {
                  return Text(
                    "Discover",
                    style: TextStyle(fontSize: 48 * size, fontWeight: FontWeight.w100, color: Const.color.discover),
                    textAlign: TextAlign.center,
                  );
                },
              ),
              SizedBox(height: 23),
              TweenAnimationBuilder(
                tween: Tween<double>(begin: -1, end: 1),
                duration: const Duration(seconds: 4),
                curve: Curves.easeInExpo,
                builder: (context, double size, _) {
                  return Text(
                    "find your music match",
                    style: TextStyle(fontSize: 14 * size, fontWeight: FontWeight.w100, color: Colors.black),
                    textAlign: TextAlign.center,
                  );
                },
              ),
              SizedBox(height: 23),
              Padding(
                padding: EdgeInsets.symmetric(horizontal: 23),
                child: Image.asset("assets/images/discover_intro.png"),
              ),
              SizedBox(height: 23),
            ],
          ),
        ));
  }
}


