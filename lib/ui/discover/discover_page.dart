import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:recordio/core/constants/constants.dart';
import 'package:recordio/ui/discover/providers/discover_list_providers.dart';
import 'package:recordio/ui/discover/states/discover_list_state.dart';
import 'package:recordio/ui/discover/pager.dart';

class DiscoverPage extends ConsumerWidget {

  static const path = "/discover/home";

  const DiscoverPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final firstTime = ref.watch(isFirstTimeStateProvider);

    return Scaffold(
      appBar: AppBar(
        title: const Text("Collaboration"),
        brightness: Brightness.light,
        backgroundColor: Colors.transparent,
        elevation: 0,
        actions: [
          Padding(
            padding: const EdgeInsets.only(right: 10.0),
            child: IconButton(
              onPressed: () {
                // Navigator.pushNamed(context, Routes.chatPage);
              },
              color: Const.color.discover,
              icon: const Icon(Icons.send),
            ),
          ),
        ],
      ),
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisSize: MainAxisSize.min,
        children: [
          Padding(
            padding: const EdgeInsets.only(left: 23),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text("Discover", style: TextStyle(fontSize: 48, fontWeight: FontWeight.w100, color: Theme.of(context).accentColor)),
                    Text(firstTime ? "Choose 3 or more artists to collaborate with" : "Choose artists to collaborate with",
                        style: const TextStyle(fontSize: 14, fontWeight: FontWeight.w100, color: Colors.black)),
                  ],
                ),
                IconButton(
                    icon: Icon(Icons.tune, color: Const.color.discover),
                    onPressed: () {
                      // navigate to filter view
                    })
              ],
            ),
          ),
          const SizedBox(height: 16),
          SizedBox(
            height: 386,
            child: Pager(),
          ),
          const SizedBox(height: 10),
          // todo to uncomment
          // Center(
          //   child: SizedBox(
          //     height: 40,
          //     width: 250,
          //     // child: NextButton(),
          //   ),
          // ),
        ],
      ),
    );
  }
}

// class NextButton extends HookWidget {
//   const NextButton({Key? key}) : super(key: key);
//
//   @override
//   Widget build(BuildContext context) {
//     final firstTime = useProvider(isFirstTimeStateProvider).state;
//     if (firstTime) {
//       final profiles$ = useProvider(discoverProvider);
//       final profiles = profiles$.profiles;
//       if (profiles$.isLoading) {
//         return SizedBox.shrink();
//       }
//
//       if (profiles!.length < 3) {
//         return GradientButton("Continuer", onPressed: () {
//           context.read(isFirstTimeStateProvider).state = false;
//           Navigator.pushNamedAndRemoveUntil(context, Routes.home, (route) => false);
//         });
//       } else {
//         if (profiles$.profilesAdded! >= 3) {
//           return GradientButton("Continuer", onPressed: () {
//             context.read(isFirstTimeStateProvider).state = false;
//             Navigator.pushNamedAndRemoveUntil(context, Routes.home, (route) => false);
//           });
//         }
//         return SizedBox.shrink();
//       }
//     } else {
//       return SizedBox.shrink();
//     }
//   }
// }
