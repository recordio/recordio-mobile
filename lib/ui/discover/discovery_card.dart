import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:recordio/core/constants/constants.dart';
import 'package:recordio/core/models/discover_profile.dart';
import 'package:recordio/core/models/sample.dart';
import 'package:recordio/core/services/services.dart';
import 'package:recordio/ui/discover/providers/discover_list_providers.dart';
import 'package:recordio/ui/discover/states/discover_item_state.dart';
import 'package:recordio/ui/discover/states/discover_list_state.dart';
import 'package:collection/collection.dart';
import 'package:recordio/core/extensions/extensions.dart';
import 'package:recordio/ui/discover/profile_details_page.dart';
import 'package:recordio/ui/discover/widgets/play_icon.dart';

class DiscoveryCard extends ConsumerWidget {
  final DiscoverItemState profileItemState;

  const DiscoveryCard({Key? key, required this.profileItemState}) : super(key: key);

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    DiscoverProfile profile = profileItemState.profile;

    return GestureDetector(
      onTap: () {
        Navigator.push(context, MaterialPageRoute(builder: (context) => ProfileDetailsPage(profile: profile)));
      },
      child: Stack(
        children: [
          SizedBox(
              height: Const.common.cardHeight,
              width: Const.common.cardWidth,
              child: Hero(
                tag: profile.id ?? "",
                child: DecoratedBox(
                  child: DecoratedBox(
                    decoration: BoxDecoration(
                      borderRadius: const BorderRadius.all(Radius.circular(7)),
                      gradient: LinearGradient(
                        begin: Alignment.bottomCenter,
                        end: Alignment.topCenter,
                        stops: const [0, .3],
                        colors: [Colors.black.withOpacity(.4), Colors.transparent],
                      ),
                    ),
                    child: Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Expanded(child: Container()),
                          Text(
                            "${profile.fullname?.capitalize()}",
                            style: const TextStyle(color: Colors.white, fontSize: 16, fontWeight: FontWeight.w600),
                          ),
                          Text(
                            "${profile.bio?.capitalize()}",
                            style: TextStyle(color: Const.color.discover, fontSize: 16, fontWeight: FontWeight.w600),
                          ),
                          Text(
                            "Paris, FR",
                            style: TextStyle(color: Colors.white, fontSize: 12, fontWeight: FontWeight.normal),
                          ),
                        ],
                      ),
                    ),
                  ),
                  position: DecorationPosition.background,
                  decoration: BoxDecoration(
                    boxShadow: [
                      BoxShadow(
                        color: Colors.black.withOpacity(.15),
                        offset: const Offset(8, 5),
                        blurRadius: 10,
                      ),
                    ],
                    borderRadius: const BorderRadius.all(Radius.circular(7)),
                    image: DecorationImage(
                      image: profile.profileImageUrl != null
                          ? CachedNetworkImageProvider(profile.profileImageUrl!)
                          : const AssetImage("assets/images/background_square.png") as ImageProvider,
                      fit: BoxFit.cover,
                    ),
                  ),
                ),
              )),
          Positioned(
            right: 16,
            bottom: 16,
            child: ClipOval(
              child: Material(
                color: Theme
                    .of(context)
                    .accentColor, // button color
                child: InkWell(
                  splashColor: Theme
                      .of(context)
                      .accentColor, // inkwell color
                  child: const SizedBox(
                    width: 60,
                    height: 60,
                    child: Icon(Icons.person_add_outlined, color: Colors.white, size: 25),
                  ),
                  onTap: () async {
                    await ref.read(discoverListProvider.notifier).request(profile);
                    // context.read(removeProfile).state = profile;
                  },
                ),
              ),
            ),
          ),
          Builder(
            builder: (context) {
              if (profile.samples?.isNotEmpty ?? false) {
                return Positioned(
                  right: 12,
                  bottom: 60,
                  child: PlayIcon(discoverItemState: profileItemState),
                );
              } else {
                return const SizedBox.shrink();
              }
            },
          )
        ],
      ),
    );
  }
}


