import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:recordio/core/models/discover_profile.dart';
import 'package:recordio/ui/discover/states/discover_list_state.dart';

class DiscoverProfileNotifier extends StateNotifier<ProfileState> {

  // todo : I have to change this line
  DiscoverProfileNotifier(String profileId) : super(ProfileState(profile: DiscoverProfile(), samples: [])) {
    fetchDetails(profileId);
  }

  fetchDetails(String profileId) {

  }


}