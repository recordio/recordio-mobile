import 'package:audioplayers/audioplayers_api.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:recordio/core/models/conversation.dart';
import 'package:recordio/core/models/discover_profile.dart';
import 'package:recordio/core/models/sample.dart';
import 'package:recordio/core/repositories/repositories.dart';
import 'package:recordio/core/services/services.dart';
import 'package:recordio/ui/chat/providers/conversation_list_providers.dart';
import 'package:recordio/ui/discover/states/discover_item_state.dart';
import 'package:recordio/ui/discover/states/discover_list_state.dart';

class DiscoverNotifier extends StateNotifier<DiscoverListState> {

  final Ref ref;

  DiscoverNotifier(this.ref) : super(DiscoverListState(itemsState: [])..loading()) {
    fetch();
    Services.audio.listenPlayerState((playerState, audioUrl) {
      state = state.copyWith(profilesState: state.itemsState.map((itemState) {
        if ((itemState.profile.samples?.isNotEmpty ?? false) && itemState.profile.samples?.first.audioUrl == audioUrl) {
          itemState = itemState.copyWith(isPlaying: playerState == PlayerState.PLAYING);
        }
        return itemState;
      }).toList());
    });
  }

  togglePlay(String profileId, String audioUrl) async {
    await Services.audio.togglePlay(audioUrl);
  }

  Future<Sample> getSample(String id) async {
    return Repositories.discoverProfiles.sample(id);
  }

  fetch() async {
    List<DiscoverProfile> profiles = await Repositories.discoverProfiles.fetch();
    state = DiscoverListState(itemsState: profiles.map((profile) => DiscoverItemState(profile: profile)).toList());
  }

  remove(String id) async {
    state.itemsState.removeWhere((itemState) => itemState.profile.id == id);
    state = state.copyWith(profilesState: [...state.itemsState], profilesAdded: state.profilesAdded! + 1);
  }

  Future<void> request(DiscoverProfile profile) async {
    Conversation? conversation = await ref.read(conversationListProvider.notifier).requestConversation(profile);
    if (conversation == null) return;

    remove(profile.id!);
  }
}