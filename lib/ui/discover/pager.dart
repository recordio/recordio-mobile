import 'package:card_swiper/card_swiper.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:recordio/core/models/discover_profile.dart';
import 'package:recordio/ui/discover/providers/discover_list_providers.dart';
import 'package:recordio/ui/discover/states/discover_item_state.dart';
import 'package:recordio/ui/discover/states/discover_list_state.dart';

import 'discovery_card.dart';

class Pager extends StatefulWidget {
  @override
  _PagerState createState() => _PagerState();
}

class _PagerState extends State<Pager> with SingleTickerProviderStateMixin {
  late AnimationController _controller;
  final SwiperController _swiperController = SwiperController();

  @override
  void initState() {
    _controller = AnimationController(vsync: this, duration: const Duration(seconds: 10));
    super.initState();
  }

  @override
  void dispose() {
    _controller.dispose();
    _swiperController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Consumer(
      builder: (context, WidgetRef ref, _) {
        final profilesState = ref.watch(discoverListProvider);
        var currentIndex = 0;
        if (profilesState.isLoading) {
          return const Center(
            child: CircularProgressIndicator(),
          );
        }
        if (profilesState.hasError) {
          return const Text("Error occured");
        }

        ref.listen<String?>(removedProfile, (previous, String? id) async {

          if(id == null) return;

          if (profilesState.itemsState.length == 1) {
            await ref.read(discoverListProvider.notifier).remove(id);
            return;
          }

          if (currentIndex + 1 == profilesState.itemsState.length) {
            await _swiperController.previous();
            await _swiperController.move(currentIndex, animation: false);
            await ref.read(discoverListProvider.notifier).remove(id);
          } else {
            await _swiperController.next();
            await _swiperController.move(currentIndex - 1, animation: false);
            await ref.read(discoverListProvider.notifier).remove(id);
          }
        });

        return Align(
          alignment: Alignment.centerLeft,
          child: Swiper(
            controller: _swiperController,
            onIndexChanged: (index) {
              currentIndex = index;
              ref.read(currentPagerIndex.notifier).state = index;
            },
            itemBuilder: (BuildContext context, int index) {
              DiscoverItemState profileItemState = profilesState.itemsState.elementAt(index);;

              if (currentIndex == index) {
                return Center(
                  child: DiscoveryCard(profileItemState: profileItemState),
                  // child: Container(height: 300, width: 300, color: Colors.pink),
                );
              }

              return Align(
                alignment: Alignment.centerLeft,
                child: DiscoveryCard(profileItemState: profileItemState),
                // child: Container(height: 300, width: 300, color: Colors.pink),
              );
            },
            loop: false,
            itemCount: profilesState.itemsState.length,
            viewportFraction: .75,
            scale: .8,
          ),
        );
      },
    );
  }
}
