import 'package:flutter/material.dart';
import 'package:recordio/core/models/discover_profile.dart';

class PreferredGenres extends StatelessWidget {

  final DiscoverProfile profile;

  const PreferredGenres(this.profile, {
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      scrollDirection: Axis.horizontal,
      child: Padding(
        padding: const EdgeInsets.symmetric(vertical: 8),
        child: Wrap(
          spacing: 15,
          children: List<Widget>.generate(profile.genres.length, (index) {
            final genre = profile.genres[index];
            return Container(
              width: 70,
              height: 30,
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: const BorderRadius.all(Radius.circular(25)),
                boxShadow: [
                  BoxShadow(color: Theme.of(context).accentColor.withOpacity(.5), blurRadius: 8, offset: Offset(0, 2))
                ]
              ),
              child: Center(
                child: Text("$genre", style: const TextStyle(color: Colors.black, fontSize: 9)),
              ),
            );
          }),
        ),
      ),
    );
  }
}
