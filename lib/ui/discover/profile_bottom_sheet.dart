import 'package:flutter/material.dart';
import 'package:recordio/core/models/discover_profile.dart';
import 'package:recordio/ui/discover/prefered_genres.dart';
import 'package:recordio/ui/discover/sample_list.dart';

class ProfileBottomSheet extends StatefulWidget {

  final DiscoverProfile profile;

  const ProfileBottomSheet(this.profile, {
    Key? key,
  }) : super(key: key);

  @override
  _ProfileBottomSheetState createState() => _ProfileBottomSheetState();
}

class _ProfileBottomSheetState extends State<ProfileBottomSheet> {
  double initialSize = .15;

  @override
  Widget build(BuildContext context) {
    final DiscoverProfile profile = widget.profile;

    return DraggableScrollableSheet(
      initialChildSize: initialSize,
      minChildSize: initialSize,
      maxChildSize: .65,
      builder: (context, scrollController) {
        return SingleChildScrollView(
          controller: scrollController,
          child: Column(
            children: [
              SizedBox(
                height: .15 * MediaQuery.of(context).size.height,
                child: Column(
                  children: [
                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 35),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Column(
                            children: [
                              Text(
                                "199",
                                style: TextStyle(color: Theme.of(context).accentColor, fontSize: 18),
                              ),
                              const Text("followers",
                                  style: TextStyle(
                                    color: Colors.white,
                                    fontSize: 12,
                                    fontWeight: FontWeight.w100,
                                  )),
                            ],
                          ),
                          Column(
                            children: [
                              Text(
                                "9",
                                style: TextStyle(
                                  color: Theme.of(context).accentColor,
                                  fontSize: 18,
                                ),
                              ),
                              const Text(
                                "published",
                                style: TextStyle(
                                  color: Colors.white,
                                  fontSize: 12,
                                  fontWeight: FontWeight.w100,
                                ),
                              ),
                            ],
                          ),
                          Column(
                            children: [
                              Text(
                                "250",
                                style: TextStyle(
                                  color: Theme.of(context).accentColor,
                                  fontSize: 18,
                                ),
                              ),
                              const Text(
                                "followings",
                                style: TextStyle(
                                  color: Colors.white,
                                  fontSize: 12,
                                  fontWeight: FontWeight.w100,
                                ),
                              ),
                            ],
                          ),
                        ],
                      ),
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Icon(
                          Icons.expand_less_rounded,
                          size: 58,
                          color: Theme.of(context).accentColor,
                        ),
                      ],
                    ),
                  ],
                ),
              ),
              Container(
                height: 700,
                width: MediaQuery.of(context).size.width,
                decoration: const BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.vertical(
                    top: Radius.circular(18),
                    bottom: Radius.circular(0),
                  ),
                ),
                child: Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 20),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      const SizedBox(height: 25),
                      // todo uncomment here
                      PreferredGenres(profile),
                      const SizedBox(height: 16),
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text("About", style: TextStyle(color: Theme.of(context).accentColor, fontSize: 18)),
                          const SizedBox(height: 12),
                          Text("${profile.bio}", style: const TextStyle(color: Colors.black, fontSize: 11)),
                        ],
                      ),
                      const SizedBox(height: 12),
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text("Songs", style: TextStyle(color: Theme.of(context).accentColor, fontSize: 18)),
                          const SizedBox(height: 12),
                          SampleList(profile)
                        ],
                      )
                    ],
                  ),
                ),
              )
            ],
          ),
        );
      },
    );
  }
}
