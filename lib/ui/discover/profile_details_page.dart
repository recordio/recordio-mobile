import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:recordio/core/models/discover_profile.dart';
import 'package:recordio/ui/discover/profile_bottom_sheet.dart';

class ProfileDetailsPage extends ConsumerWidget {
  final DiscoverProfile profile;

  const ProfileDetailsPage({required this.profile, Key? key});

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    // final firstTime = useProvider(isFirstTimeStateProvider).state;
    return SafeArea(
      top: false,
      child: Hero(
        tag: profile.id!,
        child: Scaffold(
          body: SizedBox(
            width: MediaQuery.of(context).size.width,
            height: MediaQuery.of(context).size.height,
            child: DecoratedBox(
                decoration: BoxDecoration(
                  image: DecorationImage(
                    image: profile.profileImageUrl != null
                        ? CachedNetworkImageProvider(profile.profileImageUrl!)
                        : const AssetImage("assets/images/background_square.png") as ImageProvider,
                    fit: BoxFit.cover,
                  ),
                ),
                child: Stack(
                  children: [
                    Positioned(
                      top: 0,
                      left: 0,
                      right: 0,
                      height: 200,
                      child: DecoratedBox(
                          decoration: BoxDecoration(
                        gradient: LinearGradient(
                          colors: [Colors.black.withOpacity(.9), Colors.transparent],
                          begin: Alignment.topCenter,
                          end: Alignment.bottomCenter,
                          stops: const [0, 1]
                        ),
                      )),
                    ),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        const SizedBox(height: 40),
                        IconButton(
                          icon: const Icon(Icons.arrow_back, color: Colors.white, size: 24),
                          onPressed: () {
                            Navigator.pop(context);
                          },
                        ),
                        const SizedBox(height: 20),
                        Padding(
                          padding: const EdgeInsets.symmetric(horizontal: 23),
                          child: Column(
                            children: [
                              Row(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: [
                                  Text(
                                    profile.fullname ?? "",
                                    style: const TextStyle(
                                      color: Colors.white,
                                      fontWeight: FontWeight.w800,
                                      fontSize: 18,
                                    ),
                                  ),
                                  Consumer(
                                    builder: (context, watch, _) {
                                      return Container();
                                      // todo navigate down when request conversation
                                      // final conversationsState = watch(conversationsStateProvider);
                                      // final conversation = conversationsState.items
                                      //     .firstWhereOrNull((element) => element.conversation.otherId == profile.id);
                                      // return IconButton(
                                      //   icon: Icon(conversation != null && conversation.requested ? FeatherIcons.userMinus : FeatherIcons.userPlus,
                                      //       color: Colors.white, size: 24.h),
                                      //   onPressed: () {
                                      //     context.read(conversationsStateProvider.notifier).requestConversation(profile);
                                      //   },
                                      // );
                                    },
                                  )
                                ],
                              ),
                              const SizedBox(height: 20),
                              Row(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: [
                                  IconButton(
                                    icon: const Icon(Icons.share_rounded, color: Colors.white, size: 24),
                                    padding: EdgeInsets.zero,
                                    alignment: Alignment.centerLeft,
                                    onPressed: () {},
                                  ),
                                  // todo if (!firsttime)
                                  if (true)
                                    IconButton(
                                      icon: const Icon(Icons.message_rounded, color: Colors.white, size: 24),
                                      onPressed: () {
                                        // Navigator.pushNamed(context, Routes.collaboratePage);
                                      },
                                    )
                                ],
                              ),
                              Row(
                                mainAxisAlignment: MainAxisAlignment.end,
                                children: [
                                  // todo if (!firsttime)
                                  // if (false)
                                  //   IconButton(
                                  //     icon: Icon(FeatherIcons.users, color: Colors.white, size: 24.h),
                                  //     onPressed: () {},
                                  //   )
                                ],
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                    Positioned(
                      bottom: 0,
                      left: 0,
                      right: 0,
                      height: 200,
                      child: DecoratedBox(
                          decoration: BoxDecoration(
                            gradient: LinearGradient(
                                colors: [Colors.black.withOpacity(.9), Colors.transparent],
                                begin: Alignment.bottomCenter,
                                end: Alignment.topCenter,
                                stops: const [0, 1]
                            ),
                          )),
                    ),
                    Positioned(child: ProfileBottomSheet(profile)),
                  ],
                )),
          ),
        ),
      ),
    );
  }
}
