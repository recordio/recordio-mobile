import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:recordio/ui/discover/notifiers/discover_item_notifier.dart';
import 'package:recordio/ui/discover/notifiers/discover_list_notifier.dart';
import 'package:recordio/ui/discover/states/discover_list_state.dart';

final isFirstTimeStateProvider = StateProvider<bool>((ref) => false);
final removedProfile = StateProvider<String?>((ref) => null);
final currentPagerIndex = StateProvider<int>((ref) => 0);

final discoverListProvider = StateNotifierProvider<DiscoverNotifier, DiscoverListState>((ref) => DiscoverNotifier(ref));
final discoverProfileProvider = StateNotifierProvider.autoDispose.family<DiscoverProfileNotifier, ProfileState, String>((ref, String profileId) {
  return DiscoverProfileNotifier(profileId);
});