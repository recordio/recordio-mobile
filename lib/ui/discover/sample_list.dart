import 'package:flutter/material.dart';
import 'package:recordio/core/models/discover_profile.dart';

class SampleList extends StatelessWidget {
  final DiscoverProfile profile;

  const SampleList(
    this.profile, {
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Wrap(
      spacing: 5,
      runSpacing: 5,
      children: List.generate(profile.samples?.length ?? 0, (index) {
        return Container(
          width: 50,
          height: 50,
          decoration: BoxDecoration(
            color: Colors.amber,
            borderRadius: BorderRadius.circular(7),
          ),
          child: const Icon(Icons.play_circle_outline),
        );
      }),
    );
  }
}
