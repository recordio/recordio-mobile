import 'package:recordio/core/models/discover_profile.dart';

class DiscoverItemState {
  final DiscoverProfile profile;
  final bool isPlaying;
  final String? audioUrl;

  DiscoverItemState({
    required this.profile,
    this.isPlaying = false,
    this.audioUrl,
  });

  DiscoverItemState copyWith({
    final DiscoverProfile? profile,
    final bool? isPlaying,
    final String? audioUrl,
  }) {
    return DiscoverItemState(
      profile: profile ?? this.profile,
      isPlaying: isPlaying ?? this.isPlaying,
      audioUrl: audioUrl ?? this.audioUrl,
    );
  }
}