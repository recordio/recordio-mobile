import 'package:recordio/core/models/discover_profile.dart';
import 'package:recordio/core/models/sample.dart';
import 'package:recordio/ui/discover/states/discover_item_state.dart';

import '../../../core/states/base_state.dart';

class DiscoverListState extends BaseState {
  final List<DiscoverItemState> itemsState;
  final int? profilesAdded;
  final String? profilePlayingAudio;

  DiscoverListState({
    required this.itemsState,
    this.profilesAdded = 0,
    this.profilePlayingAudio,
  });

  DiscoverListState copyWith({
    final List<DiscoverItemState>? profilesState,
    final int? profilesAdded,
    final String? profilePlayingAudio,
  }) {
    return DiscoverListState(
      itemsState: profilesState ?? itemsState,
      profilesAdded: profilesAdded ?? this.profilesAdded,
      profilePlayingAudio: profilePlayingAudio ?? this.profilePlayingAudio
    );
  }
}

class ProfileState extends BaseState {
  final DiscoverProfile profile;
  final List<Sample> samples;

  ProfileState({
    required this.profile,
    required this.samples,
  });

  ProfileState copyWith({
    final DiscoverProfile? profile,
    final List<Sample>? samples,
  }) {
    return ProfileState(
      profile: profile ?? this.profile,
      samples: samples ?? this.samples,
    );
  }
}
