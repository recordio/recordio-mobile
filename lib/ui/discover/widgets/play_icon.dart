import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:recordio/ui/discover/providers/discover_list_providers.dart';
import 'package:recordio/ui/discover/states/discover_item_state.dart';

class PlayIcon extends ConsumerWidget {

  final DiscoverItemState discoverItemState;

  const PlayIcon({
    required this.discoverItemState,
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context, WidgetRef ref) {

    return ClipOval(
      child: Material(
        color: Colors.white, // button color
        child: InkWell(
          splashColor: Colors.white, // inkwell color
          child: SizedBox(
            width: 34,
            height: 34,
            child: Center(
              child: Icon(
                discoverItemState.isPlaying ? Icons.pause : Icons.play_arrow_rounded,
                size: 30,
                color: Theme
                    .of(context)
                    .accentColor,
              ),
            ),
          ),
          onTap: () async {
            if (discoverItemState.profile.samples?.isNotEmpty ?? false) {
              String? audioUrl = discoverItemState.profile.samples?.first.audioUrl;

              if (audioUrl != null) {
                await ref.read(discoverListProvider.notifier).togglePlay(discoverItemState.profile.id!, audioUrl);
              }
            }
          },
        ),
      ),
    );
  }
}