import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:recordio/core/states/dispatcher_state.dart';

import 'start_page.dart';

class DispatcherPage extends ConsumerWidget {
  const DispatcherPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    ref.listen<AsyncValue<String>>(startupPageFutureProvider, (previous, path) {
      Navigator.pushNamedAndRemoveUntil(context, path.value ?? StartPage.path, (route) => false);
    });

    return Scaffold(
      body: Center(
        child: Container(
          color: Colors.white
        ),
      ),
    );
  }
}
