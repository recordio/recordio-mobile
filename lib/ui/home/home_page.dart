import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:recordio/core/states/bottom_navigation_state.dart';
import 'package:recordio/ui/chat/chat_home_page.dart';
import 'package:recordio/ui/collaborate/collaborate_home_page.dart';
import 'package:recordio/ui/chat/providers/conversation_list_providers.dart';
import 'package:recordio/ui/discover/discover_page.dart';
import 'package:recordio/ui/settings/settings_page.dart';
import 'package:recordio/ui/shared/colored_bottom_navigation_bar.dart';
import 'package:recordio/ui/vibes/vibes_home_page.dart';

class HomePage extends ConsumerStatefulWidget {

  static const path = 'home';

  const HomePage({Key? key}) : super(key: key);

  @override
  ConsumerState<HomePage> createState() => _HomePageState();
}

class _HomePageState extends ConsumerState<HomePage> {

  final List<Widget> pages = [
    const CollaboratePage(key: PageStorageKey('Page1')),
    const DiscoverPage(key: PageStorageKey('Page2')),
    const ChatHomePage(key: PageStorageKey('Page3')),
    const VibesHomePage(key: PageStorageKey('Page4')),
    const SettingsPage(key: PageStorageKey('Page5')),
  ];

  final PageStorageBucket bucket = PageStorageBucket();

  @override
  initState() {
    super.initState();
    ref.read(initChatProvider);
    WidgetsBinding.instance!.addPostFrameCallback((_) {
      // Services.push.requestPermissions();
    });
  }

  @override
  Widget build(BuildContext context) {
    final state = ref.watch(navigationNotifierProvider);

    return Scaffold(
        body: GestureDetector(
          onTap: () => FocusScope.of(context).requestFocus(FocusNode()),
          child: PageStorage(
            child: pages[state.currentIndex],
            bucket: bucket,
          ),
        ),
        bottomNavigationBar: const ColoredBottomNavigationBar());
  }
}
