import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:reactive_forms/reactive_forms.dart';
import 'package:recordio/core/constants/constants.dart';
import 'package:recordio/core/services/services.dart';
import 'package:recordio/ui/home/home_page.dart';
import 'package:recordio/ui/login/signup_page.dart';
import 'package:recordio/ui/onboarding/setup_profile_page.dart';
import 'package:recordio/ui/shared/gradientBackground.dart';
import 'package:recordio/ui/shared/login_button.dart';

class SigninPage extends StatelessWidget {
  static const path = '/login/signin';

  SigninPage({Key? key}) : super(key: key);

  final FormGroup signinForm = fb.group({
    'email': ['', Validators.required],
    'password': [''],
  });

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: GradientBackground(
        child: SingleChildScrollView(
          child: Column(
            children: <Widget>[
              SizedBox(height: 80),
              const Hero(
                tag: "signin",
                child: Text("Sign In",
                    style: TextStyle(
                      fontSize: 48,
                      fontWeight: FontWeight.w100,
                      color: Colors.white,
                    ),
                    textAlign: TextAlign.center),
              ),
              const Text("Login to your account",
                  style: TextStyle(
                    fontWeight: FontWeight.w200,
                    fontSize: 14,
                    color: Colors.white,
                  ),
                  textAlign: TextAlign.center),
              const SizedBox(height: 60),
              Padding(
                padding: const EdgeInsets.symmetric(vertical: 0, horizontal: 20),
                child: Card(
                  elevation: 0,
                  color: Colors.white.withOpacity(.4),
                  child: Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 22, vertical: 35),
                    child: ReactiveForm(
                      formGroup: signinForm,
                      child: Column(
                        children: <Widget>[
                          ReactiveTextField(
                            formControlName: 'email',
                            style: TextStyle(fontSize: 14, color: Theme.of(context).primaryColor),
                            decoration: InputDecoration(
                              labelText: "Email",
                              enabledBorder: UnderlineInputBorder(
                                borderSide: BorderSide(color: Theme.of(context).primaryColor),
                              ),
                            ),
                          ),
                          const SizedBox(height: 24),
                          ReactiveTextField(
                              formControlName: 'password',
                              style: TextStyle(fontSize: 14, color: Theme.of(context).primaryColor),
                              decoration: const InputDecoration(labelText: "Password"),
                              obscureText: true),
                          const SizedBox(height: 28),
                          const SizedBox(height: 26),
                          SizedBox(
                            width: 250,
                            height: 40,
                            child: LoginButton(
                              child: Text("LOGIN", style: TextStyle(fontSize: 18, color: Theme.of(context).primaryColor)),
                              onPressed: () async {
                                if (signinForm.hasErrors) return;

                                Services.work.start();

                                final form = signinForm.value;
                                String email = form['email'] as String;
                                String password = form['password'] as String;

                                try {
                                  await Services.auth.signIn(email, password);
                                  Services.work.stop();
                                  if (await Services.onboarding.isFinished) {
                                    Navigator.pushNamedAndRemoveUntil(context, HomePage.path, (route) => false);
                                  } else {
                                    Navigator.pushNamedAndRemoveUntil(context, SetupProfilePage.path, (route) => false);
                                  }
                                } catch (_) {
                                  Services.work.stop();
                                }
                              },
                            ),
                          ),
                          const SizedBox(height: 20),
                          Text.rich(
                            TextSpan(
                              style: TextStyle(color: Const.color.primary),
                              children: [
                                const TextSpan(text: "Don't have an account ?"),
                                TextSpan(
                                  recognizer: TapGestureRecognizer()
                                    ..onTap = () {
                                      Navigator.pushNamedAndRemoveUntil(context, SignupPage.path, (route) => false);
                                    },
                                  text: " Sign up",
                                  style: const TextStyle(decoration: TextDecoration.underline),
                                )
                              ],
                            ),
                          ),
//            SizedBox(height: 35)
                        ],
                      ),
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
