import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:reactive_forms/reactive_forms.dart';
import 'package:recordio/core/constants/constants.dart';
import 'package:recordio/core/services/services.dart';
import 'package:recordio/ui/login/signin_page.dart';
import 'package:recordio/ui/onboarding/setup_profile_page.dart';
import 'package:recordio/ui/shared/gradientBackground.dart';
import 'package:recordio/ui/shared/login_button.dart';

class SignupPage extends StatelessWidget {

  static const path = '/login/signup';

  SignupPage({Key? key}) : super(key: key);

  final FormGroup signupForm = fb.group({
    'email': [Validators.required, Validators.email],
    'password': [Validators.required],
    'confirmation': [Validators.required],
  });

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: GradientBackground(
          child: SingleChildScrollView(
            child: Column(
              children: <Widget>[
                const SizedBox(height: 80),
                const Text("Sign Up", style: TextStyle(fontSize: 56, fontWeight: FontWeight.w100, color: Colors.white), textAlign: TextAlign.center),
                const Text("Create a new account",
                    style: TextStyle(fontWeight: FontWeight.w200, fontSize: 16, color: Colors.white), textAlign: TextAlign.center),
                const SizedBox(height: 60),
                Padding(
                  padding: const EdgeInsets.symmetric(vertical: 0, horizontal: 20),
                  child: Card(
                    elevation: 0,
                    color: Colors.white.withOpacity(.4),
                    child: Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 22, vertical: 35),
                      child: ReactiveForm(
                        formGroup: signupForm,
                        child: Column(
                          children: <Widget>[
                            ReactiveTextField(
                              formControlName: 'email',
                              style: TextStyle(fontSize: 14, color: Theme.of(context).primaryColor),
                              decoration: const InputDecoration(hintText: "Email"),
                            ),
                            const SizedBox(height: 10),
                            ReactiveTextField(
                              formControlName: 'password',
                                style: TextStyle(fontSize: 14, color: Theme.of(context).primaryColor),
                                decoration: const InputDecoration(hintText: "Password"),
                                obscureText: true),
                            const SizedBox(height: 10),
                            ReactiveTextField(
                              formControlName: 'confirmation',
                              style: TextStyle(fontSize: 14, color: Theme.of(context).primaryColor),
                              decoration: const InputDecoration(hintText: "Confirm password"),
                              obscureText: true,
                            ),
                            const SizedBox(height: 31),
                            Row(children: <Widget>[
                              SizedBox(
                                width: 24,
                                height: 24,
                                child: Checkbox(
                                  activeColor: Theme.of(context).primaryColor,
                                  onChanged: (changed) {},
                                  value: true,
                                ),
                              ),
                              const SizedBox(width: 9),
                              Text("I Agree to terms and conditions", style: TextStyle(color: Const.color.primary))
                            ]),
                            const SizedBox(height: 26),
                            SizedBox(
                              width: 250,
                              height: 40,
                              child: LoginButton(
                                child: Text(
                                  "CREATE MY PROFILE",
                                  style: TextStyle(fontSize: 18, color: Theme.of(context).primaryColor),
                                ),
                                onPressed: () async {
                                  if (signupForm.hasErrors) return;

                                  Services.work.start();

                                  final value = signupForm.value;
                                  String email = value['email'] as String;
                                  String password = value['password'] as String;

                                  try {
                                    await Services.auth.signUp(email, password);
                                    Services.work.stop();
                                    Navigator.pushNamedAndRemoveUntil(context, SetupProfilePage.path, (route) => false);
                                  } catch (e) {
                                    Services.work.stop();
                                  }
                                },
                              ),
                            ),
                            const SizedBox(height: 21),
                            Text.rich(TextSpan(children: [
                              TextSpan(text: "Already have an account ?", style: TextStyle(color: Const.color.primary)),
                              TextSpan(
                                  recognizer: TapGestureRecognizer()
                                    ..onTap = () {
                                      Navigator.pushNamedAndRemoveUntil(context, SigninPage.path, (route) => false);
                                    },
                                  text: " Sign in",
                                  style: TextStyle(
                                    color: Const.color.primary,
                                    decoration: TextDecoration.underline,
                                  ))
                            ]))
                          ],
                        ),
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ));
  }
}
