import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:image_picker/image_picker.dart';
import 'package:recordio/core/constants/constants.dart';
import 'package:recordio/core/enums/onboarding_enum.dart';
import 'package:recordio/core/services/services.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:recordio/core/states/setup_samples_state.dart';
import 'package:recordio/core/states/upload_state.dart';
import 'package:collection/collection.dart';

class AddExtractPage extends ConsumerWidget {
  final SampleState sampleState;

  const AddExtractPage({
    Key? key,
    required this.sampleState,
  }) : super(key: key);

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final upload = ref.watch(uploadNotifier);

    return Scaffold(
      body: Builder(
        builder: (context) {
          if (upload.progression == Progression.uninitialized) {
            return const CircularProgressIndicator();
          } else if (upload.progression == Progression.uploading) {
            return Center(
              child: Padding(
                padding: EdgeInsets.symmetric(horizontal: 36),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    const Text("Uploading..."),
                    SizedBox(height: 20),
                    ClipRRect(
                      borderRadius: BorderRadius.all(Radius.circular(10)),
                      child: LinearProgressIndicator(
                        minHeight: 15,
                        value: upload.uploadRatio,
                        valueColor: const AlwaysStoppedAnimation<Color>(Colors.blueAccent),
                        backgroundColor: Colors.grey.shade200,
                      ),
                    ),
                    SizedBox(height: 20),
                    OutlineButton(
                        onPressed: () {
                          Navigator.pop(context);
                        },
                        child: const Text("Cancel")),
                  ],
                ),
              ),
            );
          } else if (upload.progression == Progression.finished) {
            return Padding(
              padding: EdgeInsets.only(left: 36, right: 36, top: 48),
              child: Column(
                children: [
                  SizedBox(height: 40),
                  Consumer(builder: (BuildContext context, WidgetRef ref, _) {
                    SampleState? state = ref.watch(samplesProvider).singleWhereOrNull((element) => element.id == sampleState.id);

                    if (state == null) {
                      return const Text("Une erreur est survenue");
                    }

                    return Row(
                      children: [
                        GestureDetector(
                          onTap: () async {
                            final picker = ImagePicker();
                            XFile? imageFile = await picker.pickImage(
                              source: ImageSource.gallery,
                              maxHeight: Const.common.imageMaxHeight,
                              maxWidth: Const.common.imageMaxWidth,
                              imageQuality: Const.common.imageQuality,
                            );
                            if (imageFile == null) return;

                            String? url = await Services.api.upload(File(imageFile.path));
                            if (url != null) {
                              state.image = imageFile;
                              state.imageUrl = url;
                              ref.read(samplesProvider.notifier).update(state);
                            } else {
                              Fluttertoast.showToast(msg: "An error occur while uploading image", backgroundColor: Colors.red);
                            }
                          },
                          child: Builder(builder: (context) {
                            if (state.image == null) {
                              return Container(
                                height: 80,
                                width: 80,
                                decoration: const BoxDecoration(
                                  borderRadius: BorderRadius.all(Radius.circular(7)),
                                  image: DecorationImage(image: AssetImage("assets/images/background_square.png"), fit: BoxFit.cover),
                                ),
                              );
                            }
                            return Container(
                              height: 80,
                              width: 80,
                              decoration: BoxDecoration(
                                borderRadius: const BorderRadius.all(Radius.circular(7)),
                                boxShadow: [BoxShadow(offset: const Offset(0, 3), blurRadius: 6, color: Colors.black.withOpacity(.4))],
                                image: DecorationImage(image: FileImage(File(state.image!.path)), fit: BoxFit.cover),
                              ),
                            );
                          }),
                        ),
                        SizedBox(width: 30),
                        FlatButton(
                          child: const Text("Change image"),
                          onPressed: () async {
                            final picker = ImagePicker();
                            XFile? imageFile = await picker.pickImage(
                              source: ImageSource.gallery,
                              maxHeight: Const.common.imageMaxHeight,
                              maxWidth: Const.common.imageMaxWidth,
                              imageQuality: Const.common.imageQuality,
                            );
                            if (imageFile == null) return;

                            String? url = await Services.api.upload(File(imageFile.path));
                            if (url != null) {
                              state.image = imageFile;
                              state.imageUrl = url;
                              ref.read(samplesProvider.notifier).update(state);
                            } else {
                              Fluttertoast.showToast(msg: "An error occur while uploading image", backgroundColor: Colors.red);
                            }
                          },
                        )
                      ],
                    );
                  }),
                  SizedBox(height: 30),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      const Text("Sample title"),
                      const SizedBox(height: 8,),
                      TextFormField(
                        initialValue: sampleState.title,
                        decoration: const InputDecoration(
                            filled: true,
                            border: OutlineInputBorder(borderSide: BorderSide.none),
                            enabledBorder: OutlineInputBorder(borderSide: BorderSide.none),
                            hintText: "Title"),
                        onChanged: (text) {
                          ref.read(samplesProvider.notifier).update(sampleState..title = text);
                        },
                      ),
                    ],
                  ),
                  const SizedBox(height: 36),
                  Padding(
                    padding: EdgeInsets.only(bottom: 36),
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.end,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        OutlineButton(
                          child: const Text("Continue"),
                          onPressed: () {
                            Navigator.pop(context);
                          },
                        ),
                      ],
                    ),
                  )
                ],
              ),
            );
          } else {
            return Center(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  const Text("An error occured"),
                  OutlineButton(
                    onPressed: () async {
                      await upload.retry();
                    },
                    child: const Text("Retry"),
                  )
                ],
              ),
            );
          }
        },
      ),
    );
  }
}
