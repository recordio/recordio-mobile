import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:image_picker/image_picker.dart';
import 'package:reactive_forms/reactive_forms.dart';
import 'package:recordio/core/constants/constants.dart';
import 'package:recordio/core/services/services.dart';
import 'package:recordio/core/states/setup_profile_state.dart';
import 'package:recordio/ui/onboarding/setup_music_preferences_page.dart';
import 'package:recordio/ui/onboarding/setup_samples_page.dart';
import 'package:recordio/ui/shared/gradient_button.dart';

class ProfilePicture extends ConsumerWidget {
  const ProfilePicture({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    SetupProfileState state = ref.watch(setupProfileProvider);
    final imageProvider = state.user?.profilePictureUrl != null
        ? NetworkImage(state.user!.profilePictureUrl!)
        : state.localProfilePath != null
            ? FileImage(File(state.localProfilePath!))
            : const AssetImage("assets/images/background_square.png") as ImageProvider;
    return Stack(
      children: <Widget>[
        Container(
          height: 110,
          width: 110,
          clipBehavior: Clip.antiAlias,
          decoration: BoxDecoration(
            shape: BoxShape.circle,
            image: DecorationImage(image: imageProvider, fit: BoxFit.cover),
          ),
        ),
        Positioned(
          bottom: 0,
          right: 0,
          child: GestureDetector(
            onTap: () async {
              final filePath = await pickImage();
              if (filePath != null) {
                ref.watch(setupProfileProvider.notifier).profilePicture(filePath);
              }
            },
            child: Container(
              height: 40,
              width: 40,
              clipBehavior: Clip.antiAlias,
              decoration: const BoxDecoration(
                color: Colors.blue,
                shape: BoxShape.circle,
                image: DecorationImage(
                  image: AssetImage("assets/images/background_square.png"),
                  fit: BoxFit.cover,
                ),
              ),
              child: const Icon(Icons.add_a_photo, color: Colors.white),
            ),
          ),
        ),
      ],
    );
  }

  Future<String?> pickImage() async {
    final picker = ImagePicker();
    XFile? pickedImage = await picker.pickImage(
      source: ImageSource.gallery,
      maxHeight: Const.common.imageMaxHeight,
      maxWidth: Const.common.imageMaxWidth,
      imageQuality: Const.common.imageQuality,
    );
    return pickedImage?.path;
  }
}
