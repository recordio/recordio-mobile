import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:recordio/core/models/sample.dart';
import 'package:recordio/core/states/setup_samples_state.dart';
import 'package:collection/collection.dart';

class SampleFilled extends ConsumerWidget {
  final SampleState sampleState;

  const SampleFilled({
    Key? key,
    required this.sampleState,
  }) : super(key: key);

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    return Column(
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Container(
              height: 50,
              width: 50,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(7),
                image: sampleState.image != null
                    ? DecorationImage(image: FileImage(File(sampleState.image!.path)), fit: BoxFit.cover)
                    : sampleState.sample.imageUrl != null
                        ? DecorationImage(image: NetworkImage(sampleState.sample.imageUrl ?? ""), fit: BoxFit.cover)
                        : const DecorationImage(image: AssetImage('assets/images/background_square.png'), fit: BoxFit.cover),
              ),
              child: PlayerButton(sampleState: sampleState),
            ),
            SizedBox(width: 25),
            SizedBox(
              width: 202,
              child: Column(children: [
                Row(
                  children: [
                    Text(sampleState.title ?? ""),
                    const Spacer(),
                    const Text("2.30"),
                  ],
                ),
                SizedBox(height: 12),
                ClipRRect(
                  borderRadius: const BorderRadius.horizontal(left: Radius.circular(5), right: Radius.circular(5)),
                  child: LinearProgressIndicator(
                    backgroundColor: const Color(0xffC4C4C4),
                    value: 0,
                    minHeight: 8,
                  ),
                )
              ]),
            ),
          ],
        ),
        SizedBox(height: 25)
      ],
    );
  }
}

class PlayerButton extends ConsumerWidget {
  final SampleState sampleState;

  const PlayerButton({
    Key? key,
    required this.sampleState,
  }) : super(key: key);

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    return Consumer(builder: (BuildContext context, WidgetRef ref, _) {
      SampleState? state = ref.watch(samplesProvider).singleWhereOrNull((s) => s.id == sampleState.id);
      final icon = state?.isPlaying ?? false ? Icons.pause : Icons.play_circle_outline;
      return IconButton(
        icon: Icon(icon, color: Colors.white, size: 36),
        onPressed: () {
          state?.isPlaying ?? false
              ? ref.read(samplesProvider.notifier).stopSample(state ?? sampleState)
              : ref.read(samplesProvider.notifier).playSample(state ?? sampleState);
        },
      );
    });
  }
}
