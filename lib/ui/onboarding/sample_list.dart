import 'dart:io';

import 'package:file_picker/file_picker.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:recordio/core/constants/constants.dart';
import 'package:recordio/core/models/sample.dart';
import 'package:recordio/core/routes/routes.dart';
import 'package:recordio/core/services/services.dart';
import 'package:recordio/core/states/setup_samples_state.dart';
import 'package:recordio/core/states/upload_state.dart';
import 'package:recordio/ui/onboarding/sample_filled.dart';

import 'add_extract.dart';

class SampleList extends ConsumerWidget {
  const SampleList({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    List<SampleState> states = ref.watch(samplesProvider);
    return Center(
      child: Column(
          children: List<Widget>.generate(states.length, (index) {
        SampleState state = states[index];
        if (state.audio != null || state.sample.audioUrl != null) {
          return SampleFilled(sampleState: state);
        } else {
          return Column(
            children: [
              Container(
                height: 61,
                width: 61,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(7),
                  image: const DecorationImage(image: AssetImage("assets/images/background_square.png")),
                ),
                child: IconButton(
                  icon: const Icon(Icons.add),
                  iconSize: 40,
                  color: Colors.white,
                  onPressed: () async {
                    Services.work.start();
                    final result = await FilePicker.platform.pickFiles(
                      type: FileType.custom,
                      allowedExtensions: Const.common.audioSupported,
                    );

                    if (result == null) {
                      Services.work.stop();
                      return;
                    }

                    File audioFile = File(result.files.first.path!);
                    state.audio = audioFile;
                    ref.read(samplesProvider.notifier).update(state);
                    String? url = await ref.read(uploadNotifier).upload(audioFile);
                    if (url != null) {
                      state.audioUrl = url;
                      addSampleInfo(context, state);
                      Services.work.stop();
                    } else {
                      Fluttertoast.showToast(msg: "An error occur while uploading the sample", backgroundColor: Colors.red);
                      Services.work.stop();
                    }
                  },
                ),
              ),
              index != 2 ? SizedBox(height: 25) : Container()
            ],
          );
        }
      })),
    );
  }

  addSampleInfo(BuildContext context, SampleState state) {
    Navigator.push(
      context,
      Routes.push(AddExtractPage(sampleState: state), direction: PushDirection.up, duration: const Duration(milliseconds: 700)),
    );
  }
}
