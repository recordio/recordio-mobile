import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:recordio/core/constants/constants.dart';
import 'package:recordio/core/services/services.dart';
import 'package:recordio/core/states/setup_music_preferences_state.dart';
import 'package:recordio/ui/onboarding/setup_samples_page.dart';
import 'package:recordio/ui/shared/gradient_button.dart';

class SetupMusicPreferencesPage extends ConsumerWidget {
  static const path = "/setup/preferences";

  const SetupMusicPreferencesPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final musicPreferencesState = ref.watch(musicPreferencesProvider);

    ref.listen(musicPreferencesProvider, (previous, List<MusicPreferenceState> next) {

    });

    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        backgroundColor: Colors.transparent,
        iconTheme: const IconThemeData(color: Colors.black),
      ),
      body: Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          SizedBox(height: 91 - Const.common.headerSize),
          const Text(
            "My Profile",
            style: TextStyle(fontSize: 48, fontWeight: FontWeight.w100, color: Colors.black),
            textAlign: TextAlign.center,
          ),
          const Text("My music preferences"),
          const SizedBox(height: 7),
          Expanded(
            child: SizedBox(
              height: 325,
              child: Padding(
                padding: const EdgeInsets.symmetric(horizontal: 50),
                child: GridView.count(
                  padding: const EdgeInsets.all(0),
                  crossAxisSpacing: 10,
                  mainAxisSpacing: 5,
                  crossAxisCount: 2,
                  childAspectRatio: 120 / 50,
                  children: List.generate(musicPreferencesState.length, (index) {
                    final state = musicPreferencesState[index];
                    return GestureDetector(
                      onTap: () {
                        ref.read(musicPreferencesProvider.notifier).select(state);
                      },
                      child: Container(
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(7),
                          image: DecorationImage(
                            image: const AssetImage("assets/images/background_square.png"),
                            fit: BoxFit.cover,
                            colorFilter: state.selected ?  null : ColorFilter.mode(Colors.white.withOpacity(.8), BlendMode.hardLight),
                          ),
                        ),
                        child: Center(
                          child: Text(
                            "${state.genre.name}",
                            style: TextStyle(
                              color: state.selected ? Colors.white : Colors.black,
                              fontSize: 14,
                            ),
                          ),
                        ),
                      ),
                    );
                  }),
                ),
              ),
            ),
          ),
          const SizedBox(height: 67),
          TweenAnimationBuilder(
            tween: Tween<double>(begin: 0, end: 1),
            duration: const Duration(milliseconds: 500),
            curve: Curves.easeInOutExpo,
            builder: (context, double size, _) {
              return SizedBox(
                height: 40 * size,
                width: 250 * size,
                child: GradientButton(
                  'NEXT',
                  onPressed: () async {
                    // Services.onboarding.currentState = Onboarding.genreSetup;
                    Services.work.start();
                    await ref.read(musicPreferencesProvider.notifier).saveGenreSelected();
                    Navigator.pushNamed(context, SetupSamplesPage.path);
                    Services.work.stop();
                  },
                ),
              );
            },
          ),
          SizedBox(height: 64),
        ],
      ),
    );
  }
}
