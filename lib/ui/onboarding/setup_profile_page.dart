import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:image_picker/image_picker.dart';
import 'package:reactive_forms/reactive_forms.dart';
import 'package:recordio/core/constants/constants.dart';
import 'package:recordio/core/services/services.dart';
import 'package:recordio/core/states/setup_profile_state.dart';
import 'package:recordio/ui/onboarding/setup_music_preferences_page.dart';
import 'package:recordio/ui/onboarding/setup_samples_page.dart';
import 'package:recordio/ui/shared/gradient_button.dart';

import 'profile_picture.dart';

class SetupProfilePage extends ConsumerWidget {
  static const path = '/setup/profile';

  const SetupProfilePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    return GestureDetector(
      onTap: () {
        FocusScope.of(context).requestFocus(FocusNode());
      },
      child: Scaffold(
        body: SizedBox.expand(
          child: SingleChildScrollView(
            child: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 45),
              child: Column(crossAxisAlignment: CrossAxisAlignment.center, children: <Widget>[
                const SizedBox(height: 91),
                const Text(
                  "My Profile",
                  style: TextStyle(fontSize: 48, fontWeight: FontWeight.w100, color: Colors.black),
                  textAlign: TextAlign.center,
                ),
                const SizedBox(height: 32),
                Stack(
                  children: [
                    Positioned.fill(
                      child: Consumer(
                        builder: (context, watch, _) {
                          return Container();
                          // return watch(setupProfileProvider.sele).maybeWhen(loading: () => const CircularProgressIndicator(), orElse: () => Container());
                        },
                      ),
                    ),
                    const ProfilePicture(),
                  ],
                ),
                const SizedBox(height: 32),
                Consumer(
                  builder: (BuildContext context, WidgetRef ref, Widget? child) {
                    final setupProfileForm = ref.watch(setupProfileFormProvider);
                    final setupProfileState = ref.watch(setupProfileProvider);
                    return ReactiveForm(
                      formGroup: setupProfileForm,
                      child: Column(
                        children: <Widget>[
                          ReactiveTextField(
                            readOnly: setupProfileState.usernameSealed,
                            formControlName: 'username',
                            style: Theme.of(context).textTheme.bodyText1,
                            decoration: const InputDecoration(
                              filled: true,
                              border: OutlineInputBorder(borderSide: BorderSide.none),
                              enabledBorder: OutlineInputBorder(borderSide: BorderSide.none),
                              hintText: "Username",
                            ),
                          ),
                          const SizedBox(height: 16),
                          ReactiveTextField(
                            formControlName: 'fullname',
                            style: Theme.of(context).textTheme.bodyText1,
                            decoration: const InputDecoration(
                                filled: true,
                                border: OutlineInputBorder(borderSide: BorderSide.none),
                                enabledBorder: OutlineInputBorder(borderSide: BorderSide.none),
                                hintText: "Full name"),
                          ),
                          const SizedBox(height: 16),
                          // Column(
                          //   children: <Widget>[
                          //     SizedBox(width: double.infinity, child: JobsSelected()),
                          //     Container(height: 1, color: Colors.black),
                          //   ],
                          // ),
                          ReactiveTextField(
                            formControlName: 'bio',
                            maxLines: 4,
                            keyboardType: TextInputType.multiline,
                            style: Theme.of(context).textTheme.bodyText1,
                            decoration: const InputDecoration(
                              filled: true,
                              border: OutlineInputBorder(borderSide: BorderSide.none),
                              enabledBorder: OutlineInputBorder(borderSide: BorderSide.none),
                              hintText: "Bio (100 characters max)",
                            ),
                          )
                        ],
                      ),
                    );
                  },
                ),
                SizedBox(height: 43),
                TweenAnimationBuilder(
                  tween: Tween<double>(begin: 0, end: 1),
                  duration: const Duration(milliseconds: 500),
                  curve: Curves.easeInOutExpo,
                  builder: (context, double size, _) {
                    return SizedBox(
                      height: 40 * size,
                      width: 300 * size,
                      child: GradientButton('Next', onPressed: () async {
                        Services.work.start();
                        FormGroup form = ref.read(setupProfileFormProvider);
                        final state = ref.read(setupProfileProvider);

                        if (form.hasErrors) {
                          Services.work.stop();
                          return;
                        }

                        if (!state.usernameSealed) {
                          bool usernameAvailable = await ref.watch(setupProfileProvider.notifier).isUsernameAvailable();
                          if (!usernameAvailable) {
                            Services.error.show("Ce nom d'utilisateur n'est pas disponible");
                            Services.work.stop();
                            return;
                          }
                        }

                        ref.watch(setupProfileProvider.notifier).save();
                        Services.work.stop();

                        await Navigator.pushNamed(context, SetupMusicPreferencesPage.path);
                      }),
                    );
                  },
                ),
              ]),
            ),
          ),
        ),
      ),
    );
  }
}

// class JobsSelected extends HookWidget {
//   @override
//   Widget build(BuildContext context) {
//     final jobsSelected = useProvider(jobsSelectedProvider);
//     if (jobsSelected.length <= 0) {
//       return FlatButton.icon(
//         onPressed: () async {
//           await Navigator.pushNamed(context, Routes.selectJobsPage);
//         },
//         icon: Icon(Icons.add),
//         label: Text('Add function'),
//       );
//     }
//     return SingleChildScrollView(
//       scrollDirection: Axis.horizontal,
//       child: Wrap(
//         children: List<Widget>.generate(jobsSelected.length + 1, (index) {
//           if (index == 0) {
//             return FlatButton.icon(
//               onPressed: () async {
//                 await Navigator.pushNamed(context, Routes.selectJobsPage);
//               },
//               icon: Icon(Icons.add),
//               label: Text('Add function'),
//             );
//           }
//
//           final job = jobsSelected[index - 1];
//           return Chip(
//               label: Text(job.name, style: TextStyle(color: Theme.of(context).primaryColor)),
//               onDeleted: () {
//                 context.read(jobsSelectedStateProvider.notifier).select(index);
//               });
//         }).toList(),
//       ),
//     );
//   }
// }
//
