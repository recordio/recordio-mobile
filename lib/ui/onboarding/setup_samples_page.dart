import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:recordio/core/constants/constants.dart';
import 'package:recordio/core/services/services.dart';
import 'package:recordio/core/states/setup_samples_state.dart';
import 'package:recordio/core/states/work_state.dart';
import 'package:recordio/ui/onboarding/sample_list.dart';
import 'package:recordio/ui/onboarding/setup_summary.dart';
import 'package:recordio/ui/shared/gradient_button.dart';

class SetupSamplesPage extends ConsumerWidget {
  static const path = "/setup/samples";

  const SetupSamplesPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context, WidgetRef ref) {

    return Scaffold(
        appBar: AppBar(
          elevation: 0,
          backgroundColor: Colors.transparent,
          iconTheme: const IconThemeData(color: Colors.black),
        ),
        body: SingleChildScrollView(
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              SizedBox(height: 91 - Const.common.headerSize),
              const Text(
                "My Profile",
                style: TextStyle(fontSize: 48, fontWeight: FontWeight.w100, color: Colors.black),
                textAlign: TextAlign.center,
              ),
              const SizedBox(height: 77),
              const Text("My music (3 first extracts)"),
              const SizedBox(height: 33),
              const SampleList(),
              const SizedBox(height: 60),
              TweenAnimationBuilder(
                tween: Tween<double>(begin: 0, end: 1),
                duration: const Duration(milliseconds: 500),
                curve: Curves.easeInOutExpo,
                builder: (context, double size, _) {
                  return SizedBox(
                    height: 40 * size,
                    width: 250 * size,
                    child: GradientButton('NEXT', onPressed: () async {
                      // todo create samples
                      Services.work.start();
                      try {
                        await ref.read(samplesProvider.notifier).sendSamples();
                        // Services.onboarding.currentState = Onboarding.samplesSetup;
                        Navigator.pushNamed(context, SetupSummaryPage.path);
                        Services.work.stop();
                      } catch(e) {
                        Services.work.stop();
                      }
                    }),
                  );
                },
              ),
              ref.watch(workProvider).maybeWhen(
                    data: (working) => working ? const Center(
                      child: SizedBox(
                        height: 50,
                        width: 50,
                        child: CircularProgressIndicator(),
                      ),
                    ) : Container(),
                    orElse: () => Container(),
                  )
            ],
          ),
        )
        );
  }
}
