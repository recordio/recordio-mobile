import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:recordio/core/models/genre.dart';
import 'package:recordio/core/services/services.dart';
import 'package:recordio/core/states/setup_music_preferences_state.dart';
import 'package:recordio/core/states/setup_profile_state.dart';
import 'package:recordio/ui/discover/discover_intro.dart';
import 'package:recordio/ui/onboarding/sample_list.dart';
import 'package:recordio/ui/shared/gradient_button.dart';
import 'package:recordio/ui/shared/options.dart';

class SetupSummaryPage extends ConsumerWidget {
  static const path = "/setup/summary";

  const SetupSummaryPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    SetupProfileState profileState = ref.watch(setupProfileProvider);
    List<Genre> genres = ref.watch(musicPreferencesProvider.notifier).selected();
    final appBarSize = AppBar().preferredSize.height;

    return Scaffold(
      backgroundColor: const Color(0xfffafafa),
      appBar: AppBar(
        elevation: 0,
        backgroundColor: Colors.transparent,
        iconTheme: const IconThemeData(color: Colors.black),
      ),
      body: Column(
        children: [
          Expanded(
            child: SingleChildScrollView(
              child: Padding(
                padding: const EdgeInsets.symmetric(horizontal: 16),
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    SizedBox(height: 91 - appBarSize - MediaQuery.of(context).viewPadding.top),
                    const Text(
                      "My Profile",
                      style: TextStyle(fontSize: 48, fontWeight: FontWeight.w100, color: Colors.black),
                      textAlign: TextAlign.center,
                    ),
                    const Text("Summary", style: TextStyle(color: Colors.black)),
                    const SizedBox(height: 24),
                    SizedBox(
                      height: 250,
                      child: Stack(
                        children: [
                          Container(
                            margin: EdgeInsets.only(top: 60),
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(7),
                              color: Colors.white,
                            ),
                            child: Center(
                              child: Column(
                                children: [
                                  const SizedBox(height: 80),
                                  Text(profileState.user?.fullname ?? "", style: const TextStyle(color: Colors.black, fontWeight: FontWeight.bold, fontSize: 18)),
                                  const Text("Paris, France", style: TextStyle(color: Colors.black, fontSize: 12)),
                                  const SizedBox(height: 18),
                                  Text(profileState.user?.bio ?? "", style: const TextStyle(color: Colors.black)),
                                  const SizedBox(height: 18),
                                ],
                              ),
                            ),
                          ),
                          Container(
                            alignment: Alignment.topCenter,
                            child: CircleAvatar(
                              radius: 60,
                              backgroundImage: profileState.user?.profilePictureUrl != null ? CachedNetworkImageProvider(profileState.user!.profilePictureUrl!) : null,
                            ),
                          ),
                        ],
                      ),
                    ),
                    const SizedBox(height: 24),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        const Text("Music"),
                        const SizedBox(height: 8),
                        SingleChildScrollView(
                          scrollDirection: Axis.horizontal,
                          primary: false,
                          child: Wrap(
                            spacing: 15,
                            children: List<Widget>.generate(genres.length, (index) {
                              final genre = genres[index];
                              return Container(
                                width: 60,
                                height: 25,
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(4),
                                  image: const DecorationImage(
                                    image: AssetImage("assets/images/background_square.png"),
                                    fit: BoxFit.cover,
                                  ),
                                ),
                                child: Center(
                                  child: Text(genre.name ?? "", style: TextStyle(color: Colors.white, fontSize: 14)),
                                ),
                              );
                            }),
                          ),
                        ),
                      ],
                    ),
                    Column(
                      children: <Widget>[
                        SizedBox(
                          width: MediaQuery.of(context).size.width,
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              const SizedBox(height: 30),
                              Row(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: [
                                  Expanded(child: Text('Samples', style: TextStyle(color: Theme.of(context).accentColor, fontSize: 18))),
                                  Options(const ['edit', 'delete'], onPressed: (item) {
                                    switch (item) {
                                      case 'edit':
                                        Navigator.of(context).pop();
                                        Navigator.of(context).pop();
                                        break;
                                      case 'delete':
                                        // ref.read(samplesProvider.notifier).reset();
                                        break;
                                    }
                                  })
                                ],
                              ),
                              const SizedBox(height: 10),
                              // const Samples(),
                              Container(
                                padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 16),
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(7),
                                  color: Colors.white,
                                ),
                                child: const SampleList(),
                              ),
                            ],
                          ),
                        ),
                        const SizedBox(height: 20)
                      ],
                    )
                  ],
                ),
              ),
            ),
          ),
          SizedBox(
            height: 80,
            child: TweenAnimationBuilder(
              tween: Tween<double>(begin: 0, end: 1),
              duration: const Duration(milliseconds: 500),
              curve: Curves.easeInOut,
              builder: (context, double size, _) {
                return SizedBox(
                  width: MediaQuery.of(context).size.width,
                  height: 70 * size,
                  child: Container(
                    color: Colors.white,
                    child: Align(
                      alignment: Alignment.center,
                      child: SizedBox(
                        height: 40,
                        width: 250,
                        child: GradientButton(
                          'LET\'S GO',
                          onPressed: () {
                            Services.onboarding.isFinished = Future.value(true);
                            Navigator.pushNamedAndRemoveUntil(context, DiscoverIntro.path, (route) => false);
                          },
                        ),
                      ),
                    ),
                  ),
                );
              },
            ),
          ),
        ],
      ),
    );
  }
}
