import 'package:flutter/material.dart';
import 'package:recordio/core/services/services.dart';

class SettingsPage extends StatelessWidget {

  static const path = "/settings";

  const SettingsPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.only(top: 50),
          child: Center(
            child: ElevatedButton(
              onPressed: () {
                Services.auth.signOut(context);
              },
              style: ElevatedButton.styleFrom(primary: Colors.pink),
              child: const Text("Déconnexion", style: TextStyle(fontSize: 30)),
            ),
          ),
        ),
      ),
    );
  }
}
