import "package:flutter/material.dart";
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:recordio/core/states/bottom_navigation_state.dart';

class ColoredBottomNavigationBar extends ConsumerWidget {

  const ColoredBottomNavigationBar({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final navigationState = ref.watch(navigationNotifierProvider);

    return Container(
      width: MediaQuery.of(context).size.width,
      decoration: const BoxDecoration(image: DecorationImage(image: AssetImage("assets/images/record_navigation_item.png"), fit: BoxFit.fill)),
      child: BottomNavigationBar(
        showSelectedLabels: false,
        showUnselectedLabels: false,
        backgroundColor: navigationState.backgroundColor,
        currentIndex: navigationState.currentIndex,
        items: getItems(navigationState),
        type: BottomNavigationBarType.fixed,
        onTap: (index) => ref.read(navigationNotifierProvider.notifier).selectedItem(index),
      ),
    );
  }

  getItems(NavigationBarStateModel state) {
    return <BottomNavigationBarItem>[
      BottomNavigationBarItem(
          label: "Collaborate", icon: Image.asset("assets/images/collaborate.png", height: 33, width: 33, color: getColor(0, state))),
      BottomNavigationBarItem(
          label: "Discover", icon: Image.asset("assets/images/user_add.png", height: 33, width: 33, color: getColor(1, state))),
      BottomNavigationBarItem(label: "Chat", icon: Image.asset("assets/images/message_circle.png", height: 33, width: 33, color: getColor(2, state))),
      BottomNavigationBarItem(
          label: "Vibes", icon: Image.asset("assets/images/activity.png", height: 33, width: 33, color: getColor(3, state))),
      BottomNavigationBarItem(
          label: "Parameters", icon: Image.asset("assets/images/settings.png", height: 33, width: 33, color: getColor(4, state))),
    ];
  }
}

getColor(int itemIndex, NavigationBarStateModel provider) {
  if (itemIndex == provider.currentIndex) {
    return provider.selectedColor;
  } else {
    return provider.unSelectedColor;
  }
}
