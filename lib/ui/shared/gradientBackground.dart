import 'package:flutter/material.dart';

class GradientBackground extends StatelessWidget {
  final Widget? child;

  GradientBackground({this.child});

  @override
  Widget build(BuildContext context) {
    return Container(
      height: double.infinity,
      width: double.infinity,
      decoration: BoxDecoration(
        image: DecorationImage(
          image: AssetImage("assets/images/background_square.png"),
          fit: BoxFit.fill,
        ),
      ),
      child: child,
    );
  }
}
