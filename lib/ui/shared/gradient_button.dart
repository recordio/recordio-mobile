import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:recordio/core/states/work_state.dart';

class GradientButton extends ConsumerWidget {
  final Widget? child;
  final Function()? onPressed;
  final String text;

  const GradientButton(this.text, {required this.onPressed, this.child, Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    return Consumer(
      builder: (BuildContext context, WidgetRef ref, Widget? child) {
        final work = ref.watch(workProvider);
        return work.maybeWhen(
          data: (bool working) => working ? const WorkingButton() : DefaultButton(onPressed: onPressed, text: text),
          orElse: () => DefaultButton(onPressed: onPressed, text: text),
        );
      },
    );
  }
}

class DefaultButton extends StatelessWidget {
  const DefaultButton({
    Key? key,
    required this.onPressed,
    required this.text,
  }) : super(key: key);

  final Function()? onPressed;
  final String text;

  @override
  Widget build(BuildContext context) {
    return RaisedButton(
      padding: const EdgeInsets.all(0),
      onPressed: onPressed,
      child: Container(
        constraints: BoxConstraints.expand(height: 80, width: 500),
        decoration: const BoxDecoration(image: DecorationImage(image: AssetImage("assets/images/button_background.png"), fit: BoxFit.fill)),
        child: Center(
          child: Text(text, style: const TextStyle(color: Colors.white)),
        ),
      ),
    );
  }
}

class WorkingButton extends StatelessWidget {
  const WorkingButton({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return RaisedButton(
      padding: const EdgeInsets.all(0),
      onPressed: null,
      child: Container(
        constraints: const BoxConstraints.expand(height: 80, width: 500),
        decoration: const BoxDecoration(image: DecorationImage(image: AssetImage("assets/images/button_background.png"), fit: BoxFit.fill)),
        child: const Center(
          child: CircularProgressIndicator(),
        ),
      ),
    );
  }
}

class DisabledButton extends StatelessWidget {
  const DisabledButton({
    Key? key,
    required this.text,
  }) : super(key: key);

  final String text;

  @override
  Widget build(BuildContext context) {
    return RaisedButton(
      padding: const EdgeInsets.all(0),
      onPressed: null,
      child: Opacity(
        opacity: .4,
        child: Container(
          constraints: BoxConstraints.expand(height: 80, width: 500),
          decoration: const BoxDecoration(
            image: DecorationImage(
              image: AssetImage("assets/images/button_background.png"),
              fit: BoxFit.fill,
              colorFilter: ColorFilter.mode(Colors.grey, BlendMode.lighten),
            ),
          ),
          child: Center(child: Text(text, style: TextStyle(color: Colors.white))),
        ),
      ),
    );
  }
}
