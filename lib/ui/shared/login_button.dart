import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:recordio/core/states/work_state.dart';

class LoginButton extends ConsumerWidget {
  final Widget child;
  final Function() onPressed;

  const LoginButton({required this.onPressed, required this.child, Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final work = ref.watch(workProvider);

    return ElevatedButton(
      style: ElevatedButton.styleFrom(
        primary: Colors.white,
      ),
      onPressed: onPressed,
      child: work.maybeWhen(
        data: (working) {
          return working ? const CircularProgressIndicator() : child;
        },
        orElse: () => child,
      ),
    );
  }
}
