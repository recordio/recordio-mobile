import 'package:flutter/material.dart';
import 'package:modal_bottom_sheet/modal_bottom_sheet.dart';

class Options extends StatefulWidget {
  final List<String> items;
  final void Function(String item) onPressed;

  Options(this.items, {required this.onPressed});

  @override
  _OptionsState createState() => _OptionsState();
}

class _OptionsState extends State<Options> {
  bool isOpened = false;

  open() {
    setState(() {
      isOpened = true;
    });
  }

  close() {
    setState(() {
      isOpened = false;
    });
  }

  @override
  Widget build(BuildContext context) {
    final items = widget.items;
    final callback = widget.onPressed;
    return IconButton(
      icon: Icon(Icons.more_horiz),
      onPressed: () {
        showMaterialModalBottomSheet(
            context: context,
            builder: (context) {
              return Column(
                mainAxisSize: MainAxisSize.min,
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,
                children: List.generate(items.length + 1, (index) {
                  if (items.length == index) {
                    return ListTile(
                      title: Text("Close", style: TextStyle(fontWeight: FontWeight.bold, fontSize: 18)),
                      onTap: () async {
                        Navigator.pop(context);
                      },
                    );
                  }
                  final item = items[index];

                  return ListTile(
                    title: Text(item),
                    onTap: () async {
                      callback(item);
                      Navigator.pop(context);
                    },
                  );
                }),
              );
            });
      },
    );
  }
}
