import 'package:flutter/material.dart';
import 'package:recordio/core/routes/routes.dart';
import 'package:recordio/ui/login/signin_page.dart';
import 'package:recordio/ui/login/signup_page.dart';
import 'package:recordio/ui/shared/gradientBackground.dart';

class StartPage extends StatelessWidget {
  static const path = "/startup";

  const StartPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: GradientBackground(
        child: SingleChildScrollView(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              const SizedBox(height: 72),
              const Text("Join the community", textAlign: TextAlign.center, style: TextStyle(fontSize: 24, color: Colors.white)),
              const SizedBox(height: 38),
              Image.asset("assets/images/logo.png", height: 186, width: 186),
              const SizedBox(height: 92),
              SizedBox(
                width: 250,
                height: 40,
                child: OutlinedButton(
                  style: OutlinedButton.styleFrom(
                    side: const BorderSide(color: Colors.white),
                    primary: Colors.white,
                  ),
                  child: const Hero(tag: "signup", child: Text("SIGN UP", style: TextStyle(fontSize: 14))),
                  onPressed: () {
                    Navigator.pushNamedAndRemoveUntil(context, SignupPage.path, (route) => false);
                  },
                ),
              ),
              SizedBox(height: 39),
              SizedBox(
                height: 40,
                width: 250,
                child: ElevatedButton(
                    style: ElevatedButton.styleFrom(
                      primary: Colors.white,
                    ),
                    onPressed: () {
                      Navigator.pushNamedAndRemoveUntil(context, SigninPage.path, (route) => false);
                    },
                    child: Text("SIGN IN", style: TextStyle(fontSize: 14, color: Theme.of(context).primaryColor))),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
