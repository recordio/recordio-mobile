import 'dart:math';

import 'package:audioplayers/audioplayers.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:recordio/core/models/vibe.dart';

class AudioFrame extends ConsumerWidget {
  AudioFrame(this.vibe, {Key? key}) : super(key: key) {
    player.setUrl(vibe.url!);
  }

  final player = AudioPlayer();
  final Vibe vibe;

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    // final audioState = useState(player.state);
    // final duration = useState(-1);
    // player.onDurationChanged.listen((d) {
    //   duration.value = d.inSeconds;
    // });
    // player.onPlayerStateChanged.listen((state) {
    //   audioState.value = state;
    // });

    return Container(
      margin: const EdgeInsets.symmetric(horizontal: 0, vertical: 50),
      child: Row(
        children: [
          // IconButton(
          //     icon: Icon(audioState.value == AudioPlayerState.PLAYING ? Icons.pause_circle_filled_rounded : Icons.play_circle_fill_rounded),
          //     iconSize: 64,
          //     onPressed: () async {
          //       print(audioState.value);
          //       if (audioState.value == AudioPlayerState.PLAYING) {
          //         await player.pause();
          //       } else {
          //         if (audioState.value == AudioPlayerState.PAUSED) {
          //           await player.resume();
          //         } else {
          //           final x = await player.play(vibe.url!);
          //           print(x);
          //         }
          //       }
          //     }),
          // if (duration.value != -1)
          //   Flexible(
          //     flex: 0,
          //     child: FittedBox(
          //       child: AudioWave(
          //         width: 5 * duration.value.toDouble(),
          //         spacing: 2.5,
          //         animation: false,
          //         bars: List.generate(duration.value, (index) => AudioWaveBar(height: 70 * Random().nextDouble(), color: Colors.black)),
          //       ),
          //     ),
          //   ),
        ],
      ),
    );
  }
}
