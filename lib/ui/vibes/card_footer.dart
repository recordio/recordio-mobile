import 'package:flutter/material.dart';
import 'package:recordio/core/constants/constants.dart';
import 'package:recordio/core/models/vibe.dart';

class CardFooter extends StatelessWidget {
  final Vibe vibe;

  const CardFooter({required this.vibe, Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.symmetric(vertical: 10),
      decoration: const BoxDecoration(
        image: DecorationImage(image: AssetImage("assets/images/background_square.png"), fit: BoxFit.cover),
      ),
      child: Row(
        children: [
          Row(
            children: [
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 8),
                child: Column(
                  children: [
                    IconButton(
                      constraints: const BoxConstraints(maxHeight: 48),
                      alignment: Alignment.bottomCenter,
                      padding: EdgeInsets.zero,
                      icon: Icon(vibe.liked! ? Icons.favorite : Icons.favorite_border_rounded),
                      color: Colors.white,
                      onPressed: () async {
                        // await context.read(vibeListProvider.notifier).like(vibe);
                      },
                    ),
                    Text("${vibe.likes}", style: Const.style.like.copyWith(color: Colors.white)),
                  ],
                ),
              ),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 8),
                child: Column(
                  children: [
                    const Icon(Icons.insert_comment, color: Colors.white),
                    Text("12", style: Const.style.like.copyWith(color: Colors.white)),
                  ],
                ),
              ),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 8),
                child: Column(
                  children: [
                    const Icon(Icons.share_rounded, color: Colors.white),
                    Text("3", style: Const.style.like.copyWith(color: Colors.white)),
                  ],
                ),
              ),
            ],
          )
        ],
      ),
    );
  }
}
