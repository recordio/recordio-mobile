import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:recordio/core/constants/constants.dart';
import 'package:recordio/core/models/vibe.dart';
import 'package:string_extensions/string_extensions.dart';

class CardHeader extends StatelessWidget {
  final Vibe vibe;

  const CardHeader({required this.vibe, Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.only(left: Const.common.leftPadding, right: 30),
      child: Row(
        children: [
          Builder(
            builder: (context) {
              if (vibe.author?.profilePictureUrl != null) {
                return CircleAvatar(backgroundImage: CachedNetworkImageProvider("${vibe.author?.profilePictureUrl!}"));
              }
              return const CircleAvatar(backgroundImage: AssetImage("assets/images/background_square.png"));
            },
          ),
          const SizedBox(width: 12),
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text("${vibe.author?.username}", style: Const.style.normal.copyWith(color: Const.color.vibes)),
              // Text("${TimeAgo.getTimeAgo(CommonUtil.toDate(vibe.postedAt), locale: "en")}"),
            ],
          ),
          const Spacer(),
          IconButton(icon: Icon(Icons.more_horiz_outlined, color: Const.color.vibes), onPressed: () {})
        ],
      ),
    );
  }
}
