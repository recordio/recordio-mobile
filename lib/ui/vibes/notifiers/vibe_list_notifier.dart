import 'dart:io';

import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:recordio/core/models/vibe.dart';
import 'package:recordio/core/repositories/repositories.dart';
import 'package:recordio/core/services/services.dart';
import 'package:recordio/ui/vibes/states/vibe_list_state.dart';

class VibeListNotifier extends StateNotifier<VibesState> {
  VibeListNotifier(VibesState state) : super(state) {
    fetch();
  }

  fetch() async {
    try {
      final vibes = await Repositories.vibes.fetch();
      state = state.copyWith(vibes: vibes);
    } catch (e) {
      state = state.copyWith()..error(e.toString());
    }
  }

  Future<void> add(Vibe vibe) async {
    Vibe? vibeCreated = await Repositories.vibes.add(vibe);
    if (vibeCreated == null) return;
    state = state.copyWith(vibes: [vibeCreated, ...state.vibes]);
  }

  Future<void> publish(Vibe vibe) async {
    String? url = await Services.api.upload(File(vibe.url!));
    if (url == null) return;

    vibe.url = url;
    Vibe? vibeAdded = await Repositories.vibes.add(vibe);
    if (vibeAdded == null) return;

    state = state.copyWith(vibes: [vibeAdded, ...state.vibes]);
  }

  like(Vibe vibe) async {
    try {
      final vibeLiked = await Repositories.vibes.like(vibe.id!);
      final index = state.vibes.indexOf(vibe);
      final vibes = state.vibes;
      vibes[index] = vibeLiked;
      state = state.copyWith(vibes: vibes);
    } catch (e) {
      state = state.copyWith()..error(e.toString());
    }
  }
}