import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:recordio/core/models/vibe.dart';
import 'package:recordio/ui/vibes/notifiers/vibe_list_notifier.dart';
import 'package:recordio/ui/vibes/states/vibe_list_state.dart';

final vibeListProvider = StateNotifierProvider<VibeListNotifier, VibesState>((_) => VibeListNotifier(VibesState(vibes: [])..loading()));
final vibeProvider = StateProvider<Vibe>((ref) => throw UnimplementedError());
final newVibeProvider = StateProvider<Vibe>((ref) => Vibe.initial());
final videoRecordedProvider = StateProvider<String>((ref) => "");
final enableVolumeProvider = StateProvider<bool>((ref) => false);