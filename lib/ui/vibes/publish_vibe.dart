import 'dart:io';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:recordio/core/constants/constants.dart';
import 'package:recordio/core/models/auth.dart';
import 'package:recordio/core/models/vibe.dart';
import 'package:recordio/core/repositories/repositories.dart';
import 'package:recordio/core/routes/routes.dart';
import 'package:recordio/core/services/services.dart';
import 'package:recordio/ui/home/home_page.dart';
import 'package:recordio/ui/vibes/audio_frame.dart';
import 'package:recordio/ui/vibes/providers/vibe_providers.dart';
import 'package:recordio/ui/vibes/video_frame.dart';

class PublishVibe extends ConsumerWidget {

  static const route = "/vibes/publish";

  const PublishVibe({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    // final currentProfile = useProvider(currentProfileProvider);
    final textController = TextEditingController();

   return FutureBuilder(future: Services.auth.auth, builder: (context, AsyncSnapshot<Auth?> snapshot) {
     if (snapshot.connectionState == ConnectionState.done && snapshot.hasData) {
       Auth? auth = snapshot.data;
       if (auth == null) {
         // todo go to signin
         return const Center(child: Text("Error"));
       }
       Vibe newVibe = ref.watch(newVibeProvider);
       return Scaffold(
         appBar: AppBar(
           brightness: Brightness.dark,
           title: const Text("Nouveau Vibe"),
           actions: [
             TextButton(
               onPressed: () async {
                 Services.work.start();
                 newVibe = newVibe.copyWith(message: textController.text);
                 if (newVibe.url == null) return;
                 ref.read(vibeListProvider.notifier).publish(newVibe);
                 Services.work.stop();
                 Navigator.pushNamedAndRemoveUntil(context, HomePage.path, (route) => false);
               },
               child: const Text("Publier", style: TextStyle(color: Colors.white)),
             ),
           ],
         ),
         body: GestureDetector(
           onTap: () => FocusScope.of(context).requestFocus(FocusNode()),
           child: SingleChildScrollView(
             child: Padding(
               padding: const EdgeInsets.all(8.0),
               child: Container(
                 child: Column(
                   children: [
                     Row(
                       children: [
                         const CircleAvatar(
                             backgroundImage: AssetImage("assets/images/background_square.png")),
                         const SizedBox(width: 12),
                         Column(
                           crossAxisAlignment: CrossAxisAlignment.start,
                           children: [
                             // Text("${user.username.capitalize()}", style: Const.style.normal.copyWith(color: Const.color.vibes)),
                           ],
                         ),
                       ],
                     ),
                     TextFormField(
                       controller: textController,
                       maxLines: null,
                       keyboardType: TextInputType.multiline,
                       decoration: InputDecoration(hintText: "your message here"),
                     ),
                     const MediaWidget(),
                   ],
                 ),
               ),
             ),
           ),
         ),
       );
     } else if (snapshot.hasError) {
       // todo handle error here
       return const Center(child: Text("Error"));
     } else {
       return const Center(child: CircularProgressIndicator());
     }
   });
  }
}

class MediaWidget extends ConsumerWidget {
  const MediaWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    Vibe vibe = ref.watch(newVibeProvider);

    if (vibe.type == "image") {
      return Image.file(File(vibe.url!));
    } else if (vibe.type == "video") {
      return const VideoFrame();
    } else if (vibe.type == "audio") {
      // return AudioFrame(vibe);
      return const Text("audio frame");
    } else {
      return Container();
    }
  }
}
