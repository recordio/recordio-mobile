// import 'package:flutter/material.dart';
// import 'package:flutter_riverpod/flutter_riverpod.dart';
// import 'package:flutter_screenutil/flutter_screenutil.dart';
// import 'package:flutter_sound/flutter_sound.dart';
// import 'package:flutter_sound/public/flutter_sound_player.dart';
// import 'package:recordio/core/routes/routes.dart';
// import 'package:recordio/core/states/vibes_state.dart';
//
// class RecordVoice extends StatefulWidget {
//   @override
//   _RecordVoiceState createState() => _RecordVoiceState();
// }
//
// class _RecordVoiceState extends State<RecordVoice> {
//   FlutterSoundPlayer _player = FlutterSoundPlayer();
//   FlutterSoundRecorder _recorder = FlutterSoundRecorder();
//   final path = "audio.mp4";
//   String voiceRecordedPath = "";
//   bool _canPlay = false;
//   bool _isRecording = false;
//   bool _isPlaying = false;
//   int _recordDuration = 0;
//
//   initSessions() async {
//     _recorder.openAudioSession().then((recorder) {
//       recorder!.setSubscriptionDuration(Duration(seconds: 1));
//     });
//     _player.openAudioSession().then((player) {
//       player!.setSubscriptionDuration(Duration(seconds: 1));
//     });
//   }
//
//   @override
//   void initState() {
//     initSessions();
//     super.initState();
//   }
//
//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       appBar: AppBar(
//         backgroundColor: Colors.transparent,
//         elevation: 0,
//         actions: [
//           TextButton(
//             onPressed: _canPlay
//                 ? () {
//                     final pVibe = context.read(newVibeProvider);
//                     pVibe.state = pVibe.state.copyWith(url: voiceRecordedPath, type: 'audio');
//                     Navigator.pushNamed(context, Routes.publishVibe);
//                   }
//                 : null,
//             child: Text("Continuer", style: TextStyle(fontWeight: FontWeight.bold)),
//           )
//         ],
//       ),
//       body: SafeArea(
//         child: SizedBox(
//           width: 1.sw,
//           child: Column(
//             crossAxisAlignment: CrossAxisAlignment.center,
//             children: [
//               if (_recorder.isRecording)
//                 Expanded(
//                   child: Center(
//                     child: StreamBuilder<RecordingDisposition>(
//                       stream: _recorder.dispositionStream(),
//                       builder: (context, snapshot) {
//                         if (snapshot.hasData) {
//                           final duration = snapshot.data!.duration;
//                           final hours = duration.inHours.toString().padLeft(2, '0');
//                           final minutes = duration.inMinutes.remainder(60).toString().padLeft(2, '0');
//                           final seconds = duration.inSeconds.remainder(60).toString().padLeft(2, '0');
//                           final time = "$hours:$minutes:$seconds";
//                           _recordDuration = duration.inSeconds;
//                           return Text(time, style: TextStyle(fontSize: 48));
//                         }
//
//                         return Text("00:00:00", style: TextStyle(fontSize: 48));
//                       },
//                     ),
//                   ),
//                 ),
//               if (_player.isPlaying)
//                 Expanded(
//                   child: Center(
//                     child: StreamBuilder<PlaybackDisposition>(
//                       stream: _player.dispositionStream(),
//                       builder: (context, snapshot) {
//                         if (snapshot.hasData) {
//                           final duration = snapshot.data!.position;
//                           final hours = duration.inHours.toString().padLeft(2, '0');
//                           final minutes = duration.inMinutes.remainder(60).toString().padLeft(2, '0');
//                           final seconds = duration.inSeconds.remainder(60).toString().padLeft(2, '0');
//                           final time = "$hours:$minutes:$seconds";
//                           return Text(time, style: TextStyle(fontSize: 48));
//                         }
//
//                         return Text("00:00:00", style: TextStyle(fontSize: 48));
//                       },
//                     ),
//                   ),
//                 ),
//               if (!_recorder.isRecording && !_player.isPlaying) Expanded(child: Center(child: Text("00:00:00", style: TextStyle(fontSize: 48)))),
//               Flexible(
//                 flex: 0,
//                 child: Builder(
//                   builder: (context) {
//                     if (_isRecording) {
//                       return IconButton(
//                         iconSize: 64,
//                         icon: Icon(Icons.stop_circle_outlined),
//                         onPressed: () async {
//                           // record
//                           final audio = await _recorder.stopRecorder();
//                           setState(() {
//                             voiceRecordedPath = audio!;
//                             _isRecording = false;
//                             _canPlay = true;
//                           });
//                         },
//                       );
//                     }
//
//                     return Row(
//                       mainAxisAlignment: MainAxisAlignment.center,
//                       children: [
//                         IconButton(
//                           iconSize: 64,
//                           icon: Icon(Icons.mic_rounded),
//                           onPressed: () async {
//                             // record
//                             await _recorder.startRecorder(toFile: path, codec: Codec.aacMP4);
//                             setState(() {
//                               _isRecording = true;
//                               _canPlay = false;
//                             });
//                           },
//                         ),
//                         if (_canPlay)
//                           IconButton(
//                             iconSize: 64,
//                             icon: Icon(_isPlaying ? Icons.pause_circle_filled_rounded : Icons.play_circle_fill_rounded),
//                             onPressed: () async {
//                               // play voice
//                               _isPlaying
//                                   ? await _player.pausePlayer()
//                                   : await _player.startPlayer(
//                                       fromURI: voiceRecordedPath,
//                                       whenFinished: () {
//                                         setState(() {
//                                           _isPlaying = !_isPlaying;
//                                         });
//                                       });
//                               setState(() {
//                                 _isPlaying = !_isPlaying;
//                               });
//                             },
//                           ),
//                       ],
//                     );
//                   },
//                 ),
//               )
//             ],
//           ),
//         ),
//       ),
//     );
//   }
// }
