import 'package:file_picker/file_picker.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:image_picker/image_picker.dart';
import 'package:recordio/core/constants/constants.dart';
import 'package:recordio/core/models/vibe.dart';
import 'package:recordio/core/routes/routes.dart';
import 'package:recordio/ui/vibes/publish_vibe.dart';

import 'providers/vibe_providers.dart';


class Ruban extends ConsumerWidget {
  const Ruban({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    return Column(
      children: [
        Container(height: 1, color: Const.color.vibes.withOpacity(.5)),
        Row(
          children: [
            Expanded(
              child: InkWell(
                onTap: () async {
                  showModalBottomSheet(
                      context: context,
                      builder: (context) {
                        return Container(
                          padding: const EdgeInsets.symmetric(vertical: 20),
                          child: Column(
                            mainAxisSize: MainAxisSize.min,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Padding(
                                padding: const EdgeInsets.only(left: 20, bottom: 20),
                                child: Text("Ajouter une vidéo", style: Const.style.title.copyWith(fontSize: 24)),
                              ),
                              Wrap(
                                children: [
                                  ListTile(
                                    leading: const Icon(Icons.video_collection_rounded, color: Colors.blue),
                                    title: const Text("Camera"),
                                    onTap: () async {
                                      final video = await ImagePicker().getVideo(source: ImageSource.camera);
                                      if (video != null) {
                                        previewVibe(context, ref, video.path, "video");
                                      }
                                    },
                                  ),
                                  ListTile(
                                    leading: const Icon(Icons.local_movies_outlined, color: Colors.blue),
                                    title: const Text("Gallerie"),
                                    onTap: () async {
                                      final video = await ImagePicker().pickVideo(source: ImageSource.gallery);
                                      if (video != null) {
                                        previewVibe(context, ref, video.path, "video");
                                      }
                                    },
                                  ),
                                ],
                              ),
                            ],
                          ),
                        );
                      });
                },
                child: Container(
                  padding: const EdgeInsets.symmetric(vertical: 10),
                  decoration: BoxDecoration(
                    border: Border(right: BorderSide(width: 1, color: Const.color.vibes.withOpacity(.5))),
                  ),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      const Text("Vidéo", style: TextStyle(fontSize: 11)),
                      Icon(Icons.local_movies_outlined, color: Const.color.vibes),
                    ],
                  ),
                ),
              ),
            ),
            Expanded(
              child: InkWell(
                onTap: () async {
                  showModalBottomSheet(
                      context: context,
                      builder: (context) {
                        return Container(
                          padding: const EdgeInsets.symmetric(vertical: 20),
                          child: Column(
                            mainAxisSize: MainAxisSize.min,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              Text("Ajouter un audio", style: Const.style.title.copyWith(fontSize: 24)),
                              Wrap(
                                children: [
                                  ListTile(
                                    leading: const Icon(Icons.mic, color: Colors.blue),
                                    title: const Text("Enrégistrement"),
                                    onTap: () async {
                                      await Navigator.pushNamed(context, Routes.recordAudio);
                                    },
                                  ),
                                  ListTile(
                                    leading: const Icon(Icons.music_note_outlined, color: Colors.blue),
                                    title: const Text("Gallerie"),
                                    onTap: () async {
                                      final audio = await FilePicker.platform.pickFiles(type: FileType.audio);
                                      if (audio != null) {
                                        previewVibe(context, ref, audio.paths.first!, "audio");
                                      }
                                    },
                                  ),
                                ],
                              ),
                            ],
                          ),
                        );
                      });
                },
                child: Container(
                  padding: const EdgeInsets.symmetric(vertical: 10),
                  decoration: BoxDecoration(
                    border: Border(right: BorderSide(width: 1, color: Const.color.vibes.withOpacity(.5))),
                  ),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      const Text("Audio", style: TextStyle(fontSize: 11)),
                      Icon(Icons.audiotrack_outlined, color: Const.color.vibes),
                    ],
                  ),
                ),
              ),
            ),
            Expanded(
              child: InkWell(
                onTap: () async {
                  showModalBottomSheet(
                      context: context,
                      builder: (context) {
                        return Container(
                          padding: const EdgeInsets.symmetric(vertical: 20),
                          child: Column(
                            mainAxisSize: MainAxisSize.min,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              Text("Ajouter une image", style: Const.style.title.copyWith(fontSize: 24)),
                              Wrap(
                                children: [
                                  ListTile(
                                    leading: const Icon(Icons.camera, color: Colors.blue),
                                    title: const Text("Camera"),
                                    onTap: () async {
                                      // Services.work.startJob(context);
                                      // final image = await ImagePicker().getImage(source: ImageSource.camera);
                                      // Services.work.stopJob(context);
                                      // if (image != null) {
                                      //   previewVibe(context, image.path, "image");
                                      //   Services.work.stopJob(context);
                                      // }
                                    },
                                  ),
                                  ListTile(
                                    leading: const Icon(Icons.perm_media_rounded, color: Colors.blue),
                                    title: const Text("Gallerie"),
                                    onTap: () async {
                                      // Services.work.startJob(context);
                                      // final image = await ImagePicker().getImage(source: ImageSource.gallery);
                                      // Services.work.stopJob(context);
                                      // if (image != null) {
                                      //   previewVibe(context, image.path, "image");
                                      // }
                                    },
                                  ),
                                ],
                              ),
                            ],
                          ),
                        );
                      });
                },
                child: Container(
                  padding: const EdgeInsets.symmetric(vertical: 10),
                  decoration: BoxDecoration(
                    border: Border(right: BorderSide(width: 1, color: Const.color.vibes.withOpacity(.5))),
                  ),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      const Text("Image", style: TextStyle(fontSize: 11)),
                      Icon(Icons.image, color: Const.color.vibes),
                    ],
                  ),
                ),
              ),
            ),
          ],
        ),
        Container(height: 1, color: Const.color.vibes.withOpacity(.5)),
      ],
    );
  }

  void previewVibe(BuildContext context, WidgetRef ref, String path, String type) {
    Vibe vibe = ref.read(newVibeProvider);
    vibe = vibe.copyWith(url: path, type: type);
    ref.read(newVibeProvider.notifier).state = vibe;
    Navigator.pushNamed(context, PublishVibe.route);
  }
}
