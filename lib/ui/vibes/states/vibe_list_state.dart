import 'package:recordio/core/models/vibe.dart';
import 'package:recordio/core/states/base_state.dart';

class VibesState extends BaseState {
  List<Vibe> vibes = const [];

  VibesState({required this.vibes});

  VibesState copyWith({List<Vibe>? vibes}) {
    return VibesState(vibes: vibes ?? this.vibes);
  }
}
