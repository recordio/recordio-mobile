import 'package:better_player/better_player.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:recordio/core/constants/constants.dart';
import 'package:recordio/core/models/vibe.dart';
import 'package:recordio/ui/vibes/audio_frame.dart';
import 'package:recordio/ui/vibes/card_footer.dart';
import 'package:recordio/ui/vibes/card_header.dart';
import 'package:recordio/ui/vibes/providers/vibe_providers.dart';
import 'package:video_player/video_player.dart';

class VibeCard extends ConsumerWidget {
  final Vibe vibe;

  const VibeCard(this.vibe, {Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        CardHeader(vibe: vibe),
        Padding(
          padding: EdgeInsets.only(left: Const.common.leftPadding, top: 4, bottom: 4),
          child: Text("${vibe.message}", style: Const.style.normal),
        ),
        Builder(
          builder: (context) {
            switch (vibe.type) {
              case "video":
                return Column(
                  children: [VideoCard(vibe: vibe)],
                );
              case "image":
                return Column(
                  children: [
                    // ImageCard(),
                    Container(),
                  ],
                );
              case "audio":
                return Column(
                  children: [
                    // AudioCard(vibe),
                  ],
                );
              default:
                return Column(
                  children: [
                    // DefaultCard(),
                  ],
                );
            }
          },
        ),
        CardFooter(vibe: vibe)
      ],
    );
  }
}

class ImageCard extends ConsumerWidget {

  const ImageCard({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final vibe = ref.watch(vibeProvider);
    return Column(
      children: [
        Padding(
          padding: const EdgeInsets.only(top: 8.0),
          child: CachedNetworkImage(
            imageUrl: vibe.url!,
            fit: BoxFit.cover,
            width: double.infinity,
            placeholder: (context, string) {
              return const Center(
                child: CircularProgressIndicator(),
              );
            },
          ),
        ),
      ],
    );
  }
}

class VideoCard extends ConsumerStatefulWidget {

  const VideoCard({required this.vibe, Key? key}): super(key: key);

  @override
  _VideoCardState createState() => _VideoCardState();

  final Vibe vibe;
}

class _VideoCardState extends ConsumerState<VideoCard> {
  late BetterPlayerController _videoController;
  double aspectRatio = 1.0;

  @override
  void initState() {
    final videoSource = BetterPlayerDataSource(BetterPlayerDataSourceType.network, widget.vibe.url!);
    _videoController = BetterPlayerController(
      BetterPlayerConfiguration(
          looping: true,
          fit: BoxFit.cover,
          aspectRatio: 1.0,
          controlsConfiguration: BetterPlayerControlsConfiguration(
              showControls: true,
              playerTheme: BetterPlayerTheme.custom,
              customControlsBuilder: (controller, onPlayerVisibilityChangede) {
                return CustomControl(controller);
              })),
      betterPlayerDataSource: videoSource,
    )..setVolume(ref.read(enableVolumeProvider) ? 1.0 : 0.0);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {

    ref.listen(enableVolumeProvider, (bool? previous, bool next) {
      if (next) {
        _videoController.setVolume(1.0);
      } else {
        _videoController.setVolume(0.0);
      }
    });

    return Padding(
      padding: const EdgeInsets.only(top: 8.0),
      child: Stack(
        children: [
          BetterPlayer(controller: _videoController),
        ],
      ),
    );
  }
}

class CustomControl extends ConsumerWidget {
  final BetterPlayerController controller;

  const CustomControl(this.controller, {Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    return Positioned.fill(
      child: Stack(
        children: [
          Positioned.fill(
            child: GestureDetector(
              onTap: () {
                // print(context.read(enableVolumeProvider).state);
                if (controller.isPlaying() ?? false) {
                  controller.pause();
                } else {
                  controller.play();
                }
              },
            ),
          ),
          Align(
            alignment: Alignment.topRight,
            child: IconButton(
              color: Colors.white,
              iconSize: 36,
              icon: ref.watch(enableVolumeProvider) ? const Icon(Icons.volume_up) : const Icon(Icons.volume_mute),
              onPressed: () {
                final volume = ref.read(enableVolumeProvider);
                if (volume) {
                  controller.setVolume(0);
                  ref.read(enableVolumeProvider.notifier).state = false;
                } else {
                  controller.setVolume(1.0);
                  ref.read(enableVolumeProvider.notifier).state = true;
                }
              },
            ),
          ),
        ],
      ),
    );
  }
}

class AudioCard extends StatelessWidget {
  final Vibe vibe;

  const AudioCard(
    this.vibe, {
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return AudioFrame(vibe);
  }
}

class DefaultCard extends StatelessWidget {

  const DefaultCard({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container();
  }
}

class Controls extends StatefulWidget {
  const Controls({Key? key, required this.controller}) : super(key: key);

  final VideoPlayerController controller;

  @override
  _ControlsState createState() => _ControlsState();
}

class _ControlsState extends State<Controls> {
  bool isPlaying = false;

  changeState() {
    setState(() {
      if (widget.controller.value.isPlaying != isPlaying) {
        isPlaying = widget.controller.value.isPlaying;
      }
    });
  }

  @override
  void initState() {
    super.initState();
    widget.controller.addListener(changeState);
  }

  @override
  void dispose() {
    super.dispose();
    widget.controller.removeListener(changeState);
    widget.controller.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: <Widget>[
        AnimatedSwitcher(
          duration: const Duration(milliseconds: 50),
          reverseDuration: const Duration(milliseconds: 200),
          child: widget.controller.value.isPlaying
              ? const SizedBox.shrink()
              : Container(
                  color: Colors.black26,
                  child: const Center(
                    child: Icon(
                      Icons.play_arrow,
                      color: Colors.white,
                      size: 100.0,
                    ),
                  ),
                ),
        ),
        GestureDetector(
          onTap: () {
            widget.controller.value.isPlaying ? widget.controller.pause() : widget.controller.play();
          },
        ),
      ],
    );
  }
}
