import 'package:flutter/material.dart';
import 'package:recordio/core/constants/constants.dart';
import 'package:recordio/core/routes/routes.dart';
import 'package:recordio/ui/vibes/ruban.dart';
import 'package:recordio/ui/vibes/vibes_list.dart';

class VibesHomePage extends StatelessWidget {
  const VibesHomePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        appBar: AppBar(
          toolbarHeight: 35,
          brightness: Brightness.dark,
          backgroundColor: Colors.transparent,
          elevation: 0,
        ),
        body: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Padding(
                padding: EdgeInsets.only(left: Const.common.leftPadding),
                child: Text("Vibes", style: Const.style.pageTitle.copyWith(color: Const.color.vibes)),
              ),
              const SizedBox(height: 16),
              const Ruban(),
              const SizedBox(height: 16),
              VibesList(),
            ],
          ),
        ),
      ),
    );
  }
}
