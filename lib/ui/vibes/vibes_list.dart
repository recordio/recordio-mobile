import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:recordio/core/models/vibe.dart';
import 'package:recordio/ui/vibes/providers/vibe_providers.dart';
import 'package:recordio/ui/vibes/vibe_card.dart';

class VibesList extends ConsumerWidget {
  const VibesList({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final vibeListState = ref.watch(vibeListProvider);
    if (vibeListState.isLoading) {
      return const Center(child: CircularProgressIndicator());
    } else if (vibeListState.hasError) {
      return const Text("Error occured, retry again");
    }
    return ListView.separated(
      primary: false,
      shrinkWrap: true,
      itemBuilder: (context, index) {
        Vibe vibe = vibeListState.vibes.elementAt(index);
        return VibeCard(vibe);
      },
      separatorBuilder: (context, index) {
        return const SizedBox(height: 25);
      },
      itemCount: vibeListState.vibes.length,
    );
  }
}
