import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:recordio/ui/vibes/providers/vibe_providers.dart';
import 'package:video_player/video_player.dart';

class VideoFrame extends ConsumerStatefulWidget {
  const VideoFrame({Key? key}) : super(key: key);

  @override
  _VideoFrameState createState() => _VideoFrameState();
}

class _VideoFrameState extends ConsumerState<VideoFrame> {
  late VideoPlayerController _controller;

  initializeVideoController() {
    final vibe = ref.read(newVibeProvider);
    _controller = VideoPlayerController.file(File(vibe.url!))..setLooping(true);
    _controller.initialize();
    // WidgetsBinding.instance.addPostFrameCallback((_) {
    //   _controller.initialize();
    // });

    _controller.addListener(() {
      setState(() {});
    });
  }

  @override
  void initState() {
    super.initState();
    initializeVideoController();
  }

  @override
  Widget build(BuildContext context) {
    print('aspect ratio');
    print(_controller.value.aspectRatio);
    return AspectRatio(
      aspectRatio: _controller.value.aspectRatio,
      child: Stack(
        children: [
          VideoPlayer(_controller),
          _ControlsOverlay(controller: _controller),
          Positioned(width: MediaQuery.of(context).size.width, bottom: 0, child: VideoProgressIndicator(_controller, allowScrubbing: true)),
        ],
      ),
    );
  }
}

class _ControlsOverlay extends StatelessWidget {
  const _ControlsOverlay({Key? key, required this.controller}) : super(key: key);

  final VideoPlayerController controller;

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: <Widget>[
        AnimatedSwitcher(
          duration: const Duration(milliseconds: 50),
          reverseDuration: const Duration(milliseconds: 200),
          child: controller.value.isPlaying
              ? const SizedBox.shrink()
              : Container(
                  color: Colors.black26,
                  child: const Center(
                    child: Icon(
                      Icons.play_arrow,
                      color: Colors.white,
                      size: 100.0,
                    ),
                  ),
                ),
        ),
        GestureDetector(
          onTap: () {
            controller.value.isPlaying ? controller.pause() : controller.play();
          },
        ),
      ],
    );
  }
}
